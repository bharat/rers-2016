#inputs [[A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T]]
#outputs [[X, Y, Z, U, V, W]]
#0: output U, output V without output W responds to input A betwen input I and input R
(false R (! (iI & (true U iR)) | ((! iA | (! iR U (((oU & ! iR) & ! oW) & X ((! iR & ! oW) U oV)))) U iR)))

#1: output Z, output W responds to input B after input O
(false R (! iO | (false R (! iB | (oZ & X (true U oW))))))

#2: input Q precedes output W before input K
(! (true U iK) | (! oW U (iQ | iK)))

#3: input P precedes output X between input L and output U
(false R (! ((iL & ! oU) & (true U oU)) | (! oX U (iP | oU))))

#4: output W, output X responds to input E between input J and input B
(false R (! (iJ & (true U iB)) | ((! iE | (! iB U ((oW & ! iB) & X (! iB U oX)))) U iB)))

#5: output Z, output Y responds to input P between input K and input E
(false R (! (iK & (true U iE)) | ((! iP | (! iE U ((oZ & ! iE) & X (! iE U oY)))) U iE)))

#6: output U responds to input T after output W
(false R (! oW | (false R (! iT | (true U oU)))))

#7: output V, output Y responds to input D between input I and input E
(false R (! (iI & (true U iE)) | ((! iD | (! iE U ((oV & ! iE) & X (! iE U oY)))) U iE)))

#8: output Z, output Y without output X responds to input Q after input C
(false R (! iC | (false R (! iQ | ((oZ & ! oX) & X (! oX U oY))))))

#9: output V, output U without output Z responds to input D betwen input I and input M
(false R (! (iI & (true U iM)) | ((! iD | (! iM U (((oV & ! iM) & ! oZ) & X ((! iM & ! oZ) U oU)))) U iM)))

#10: output X, output Z responds to input D after input K
(false R (! iK | (false R (! iD | (oX & X (true U oZ))))))

#11: output W, output Z without output Y responds to input K after input N until input O
(false R (! iN | ((! iK | (! iO U (((oW & ! iO) & ! oY) & X ((! iO & ! oY) U oZ)))) U (iO | (false R (! iK | ((oW & ! oY) & X (! oY U oZ))))))))

#12: output Z responds to input C, input Q between input N and output V
(false R (! (iN & (true U oV)) | ((iC & (! X (! oV U iQ) | X (! oV U (iQ & (true U oZ))))) U oV)))

#13: output W responds to input Q after input P until input T
(false R (iP & (! ! iT | ((! iQ | (! iT U (oW & ! iT))) WU iT))))

#14: input C always precedes output Y
(! oY WU iC)

#15: output X responds to input M after output U
(false R (! oU | (false R (! iM | (true U oX)))))

#16: output W responds to input Q, input L after output X
(false R (! oX | (false R (iQ & (! X (true U iL) | X (! iL U (iL & (true U oW))))))))

#17: input S precedes output Y, output W between output U and output V
(false R (! (oU & (true U oV)) | (! ((oY & ! oV) & X (! oV U (oW & ! oV))) U (oV | iS))))

#18: input P, output W precedes output Y before input E
(! (true U iE) | (! oY U (iE | ((iP & ! oY) & X (! oY U oW)))))

#19: output Y, output X responds to input A after output V until input N
(false R (! oV | ((! iA | (! iN U ((oY & ! iN) & X (! iN U oX)))) U (iN | (false R (! iA | (oY & X (true U oX))))))))

#20: output U, output X without output V responds to input C betwen input B and input L
(false R (! (iB & (true U iL)) | ((! iC | (! iL U (((oU & ! iL) & ! oV) & X ((! iL & ! oV) U oX)))) U iL)))

#21: output X responds to input O, input S after input E
(false R (! iE | (false R (iO & (! X (true U iS) | X (! iS U (iS & (true U oX))))))))

#22: output Z responds to input H, input B after input G until input K
(false R (! iG | ((iH & (! X (! iK U iB) | X (! iK U (iB & (true U oZ))))) U (iK | (false R (iH & (! X (! iK U iB) | X (! iK U (iB & (true U oZ))))))))))

#23: input G precedes output V before input F
(! (true U iF) | (! oV U (iG | iF)))

#24: output U responds to input L before input A
(! (true U iA) | ((! iL | (! iA U (oU & ! iA))) U iA))

#25: output X responds to input D between input K and input O
(false R (! ((iK & ! iO) & (true U iO)) | ((! iD | (! iO U (oX & ! iO))) U iO)))

#26: input H precedes output Y, output X between input I and input J
(false R (! (iI & (true U iJ)) | (! ((oY & ! iJ) & X (! iJ U (oX & ! iJ))) U (iJ | iH))))

#27: input F precedes output X before output V
(! (true U oV) | (! oX U (iF | oV)))

#28: output W responds to input S, input L before input A
(! (true U iA) | ((iS & (! X (! iA U iL) | X (! iA U (iL & (true U oW))))) U iA))

#29: output Y, output W always responds to input P
(false R (! iP | (true U (oY & X (true U oW)))))

#30: input N, output Z always precedes output U
(! (true U oU) | (! oU U ((iN & ! oU) & X (! oU U oZ))))

#31: output W responds to input F between input H and output Z
(false R (! ((iH & ! oZ) & (true U oZ)) | ((! iF | (! oZ U (oW & ! oZ))) U oZ)))

#32: input M precedes output U before input J
(! (true U iJ) | (! oU U (iM | iJ)))

#33: output U, output Y without output X responds to input O after input N
(false R (! iN | (false R (! iO | ((oU & ! oX) & X (! oX U oY))))))

#34: input H always precedes output W
(! oW WU iH)

#35: input E precedes output Z, output X after input L
((false R ! iL) | (! iL U (iL & (! (true U (oZ & X (true U oX))) | (! oZ U iE)))))

#36: output V, output U responds to input E before input L
(! (true U iL) | ((! iE | (! iL U ((oV & ! iL) & X (! iL U oU)))) U iL))

#37: input T, output X precedes output Z before input D
(! (true U iD) | (! oZ U (iD | ((iT & ! oZ) & X (! oZ U oX)))))

#38: output X, output V without output Y responds to input P betwen output U and input E
(false R (! (oU & (true U iE)) | ((! iP | (! iE U (((oX & ! iE) & ! oY) & X ((! iE & ! oY) U oV)))) U iE)))

#39: input H, input P precedes output W before input G
(! (true U iG) | (! oW U (iG | ((iH & ! oW) & X (! oW U iP)))))

#40: output U responds to input P after input I
(false R (! iI | (false R (! iP | (true U oU)))))

#41: output V, output X without output Y responds to input C before output W
(! (true U oW) | ((! iC | (! oW U (((oV & ! oW) & ! oY) & X ((! oW & ! oY) U oX)))) U oW))

#42: input C, input T precedes output V before input H
(! (true U iH) | (! oV U (iH | ((iC & ! oV) & X (! oV U iT)))))

#43: output X, output V responds to input I between input L and input O
(false R (! (iL & (true U iO)) | ((! iI | (! iO U ((oX & ! iO) & X (! iO U oV)))) U iO)))

#44: input B, output W precedes output V after input L until input J
(false R (! iL | (! (true U oV) | (! oV U (iJ | ((iB & ! oV) & X (! oV U oW)))))))

#45: output Y precedes output U before input R
(! (true U iR) | (! oU U (oY | iR)))

#46: output Y responds to input S, input O before output V
(! (true U oV) | ((iS & (! X (! oV U iO) | X (! oV U (iO & (true U oY))))) U oV))

#47: output Z responds to input A, input P between output U and output V
(false R (! (oU & (true U oV)) | ((iA & (! X (! oV U iP) | X (! oV U (iP & (true U oZ))))) U oV)))

#48: output V precedes output W before input N
(! (true U iN) | (! oW U (oV | iN)))

#49: output Z, output Y always responds to input N
(false R (! iN | (true U (oZ & X (true U oY)))))

#50: output Y precedes output V between input G and input I
(false R (! ((iG & ! iI) & (true U iI)) | (! oV U (oY | iI))))

#51: output V, output W without output Z responds to input N before input P
(! (true U iP) | ((! iN | (! iP U (((oV & ! iP) & ! oZ) & X ((! iP & ! oZ) U oW)))) U iP))

#52: input E precedes output X after input S
((false R ! iS) | (true U (iS & (! oX WU iE))))

#53: output V, output Z without output X always responds to input B
(false R (! iB | (true U ((oV & ! oX) & X (! oX U oZ)))))

#54: output V always responds to input N
(false R (! iN | (true U oV)))

#55: input L precedes output X, output U before input N
(! (true U iN) | (! ((oX & ! iN) & X (! iN U (oU & ! iN))) U (iN | iL)))

#56: input H precedes output Y, output Z after output V
((false R ! oV) | (! oV U (oV & (! (true U (oY & X (true U oZ))) | (! oY U iH)))))

#57: output U, input B precedes output V after input Q until input H
(false R (! iQ | (! (true U oV) | (! oV U (iH | ((oU & ! oV) & X (! oV U iB)))))))

#58: output U responds to input B, input R between input J and input H
(false R (! (iJ & (true U iH)) | ((iB & (! X (! iH U iR) | X (! iH U (iR & (true U oU))))) U iH)))

#59: output V responds to input J, input E before input N
(! (true U iN) | ((iJ & (! X (! iN U iE) | X (! iN U (iE & (true U oV))))) U iN))

#60: output Y always responds to input R
(false R (! iR | (true U oY)))

#61: output Y always responds to input O
(false R (! iO | (true U oY)))

#62: input L, input H precedes output V between output Z and input K
(false R (! (oZ & (true U iK)) | (! oV U (iK | ((iL & ! oV) & X (! oV U iH))))))

#63: output U responds to input B between output Y and output W
(false R (! ((oY & ! oW) & (true U oW)) | ((! iB | (! oW U (oU & ! oW))) U oW)))

#64: input R precedes output U, output Z before output Y
(! (true U oY) | (! ((oU & ! oY) & X (! oY U (oZ & ! oY))) U (oY | iR)))

#65: output Z precedes output V, output Y between output X and input T
(false R (! (oX & (true U iT)) | (! ((oV & ! iT) & X (! iT U (oY & ! iT))) U (iT | oZ))))

#66: input A, input G precedes output V after input R
((false R ! iR) | (! iR U (iR & (! (true U oV) | (! oV U ((iA & ! oV) & X (! oV U iG)))))))

#67: output X responds to input S, input P after input L
(false R (! iL | (false R (iS & (! X (true U iP) | X (! iP U (iP & (true U oX))))))))

#68: output W, output Z always responds to input F
(false R (! iF | (true U (oW & X (true U oZ)))))

#69: output Z responds to input H before input E
(! (true U iE) | ((! iH | (! iE U (oZ & ! iE))) U iE))

#70: output Y responds to input I, input P before input J
(! (true U iJ) | ((iI & (! X (! iJ U iP) | X (! iJ U (iP & (true U oY))))) U iJ))

#71: output Z responds to input I before output V
(! (true U oV) | ((! iI | (! oV U (oZ & ! oV))) U oV))

#72: output W, input A always precedes output Z
(! (true U oZ) | (! oZ U ((oW & ! oZ) & X (! oZ U iA))))

#73: output X, output Z responds to input A before input I
(! (true U iI) | ((! iA | (! iI U ((oX & ! iI) & X (! iI U oZ)))) U iI))

#74: output Z always responds to input T
(false R (! iT | (true U oZ)))

#75: output X, output Y responds to input L between input O and input G
(false R (! (iO & (true U iG)) | ((! iL | (! iG U ((oX & ! iG) & X (! iG U oY)))) U iG)))

#76: input F always precedes output Z
(! oZ WU iF)

#77: input L precedes output X, output W after input C until input O
(false R (! iC | ((! ((oX & ! iO) & X (! iO U (oW & ! iO))) U (iO | iL)) | (false R ! (oX & X (true U oW))))))

#78: output Z, output U without output V responds to input H betwen input P and input L
(false R (! (iP & (true U iL)) | ((! iH | (! iL U (((oZ & ! iL) & ! oV) & X ((! iL & ! oV) U oU)))) U iL)))

#79: input R always precedes output W
(! oW WU iR)

#80: input S, output U precedes output Z after input J
((false R ! iJ) | (! iJ U (iJ & (! (true U oZ) | (! oZ U ((iS & ! oZ) & X (! oZ U oU)))))))

#81: input C always precedes output Y, output Z
(! (true U (oY & X (true U oZ))) | (! oY U iC))

#82: output X, output U without output Y responds to input H after input M until input K
(false R (! iM | ((! iH | (! iK U (((oX & ! iK) & ! oY) & X ((! iK & ! oY) U oU)))) U (iK | (false R (! iH | ((oX & ! oY) & X (! oY U oU))))))))

#83: output W, output Y responds to input T before input N
(! (true U iN) | ((! iT | (! iN U ((oW & ! iN) & X (! iN U oY)))) U iN))

#84: output U, output X responds to input J before input K
(! (true U iK) | ((! iJ | (! iK U ((oU & ! iK) & X (! iK U oX)))) U iK))

#85: output Y responds to input L after output Z until input T
(false R (oZ & (! ! iT | ((! iL | (! iT U (oY & ! iT))) WU iT))))

#86: output U responds to input C between output V and input H
(false R (! ((oV & ! iH) & (true U iH)) | ((! iC | (! iH U (oU & ! iH))) U iH)))

#87: input M precedes output W, output V between output U and output Z
(false R (! (oU & (true U oZ)) | (! ((oW & ! oZ) & X (! oZ U (oV & ! oZ))) U (oZ | iM))))

#88: input E, input T precedes output X between input O and input J
(false R (! (iO & (true U iJ)) | (! oX U (iJ | ((iE & ! oX) & X (! oX U iT))))))

#89: output W, output Z without output V responds to input Q after input I
(false R (! iI | (false R (! iQ | ((oW & ! oV) & X (! oV U oZ))))))

#90: input K, input B always precedes output X
(! (true U oX) | (! oX U ((iK & ! oX) & X (! oX U iB))))

#91: input O precedes output U after input E
((false R ! iE) | (true U (iE & (! oU WU iO))))

#92: output Z, output W without output X responds to input M after input L until input F
(false R (! iL | ((! iM | (! iF U (((oZ & ! iF) & ! oX) & X ((! iF & ! oX) U oW)))) U (iF | (false R (! iM | ((oZ & ! oX) & X (! oX U oW))))))))

#93: output V precedes output W before input L
(! (true U iL) | (! oW U (oV | iL)))

#94: output X, output Z without output W responds to input E after input T
(false R (! iT | (false R (! iE | ((oX & ! oW) & X (! oW U oZ))))))

#95: input O precedes output X after input S until output V
(false R (iS & (! ! oV | (! oX WU (iO | oV)))))

#96: input N always precedes output Z
(! oZ WU iN)

#97: output W, output Y responds to input O before input G
(! (true U iG) | ((! iO | (! iG U ((oW & ! iG) & X (! iG U oY)))) U iG))

#98: input O precedes output X after input T
((false R ! iT) | (true U (iT & (! oX WU iO))))

#99: output X always responds to input G
(false R (! iG | (true U oX)))

