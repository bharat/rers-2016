RERS Challenge 2016
===================

The RERS challenge 2016 (practice material) consists of two groups of program;
progams in which we have to check validity of LTL formulas, and programs in
which we have to check reachability of error states. The dot files here are
results of many hours of black box learning (with state of the art algorithms
and no modifications specific to the RERS challenge) these programs.

I am pretty confident that the first batch of dot file (1 - 9) are complete
models, as the last hypothesis was learnt within 1 day and no further counter
examples were found in the following week. Similarly for problem 10. But of
course we can never be sure with this technique!

For problems 11 - 18 I am sure we don't have complete models, as the learning
algorithm was still very active when stopped/crashed (meaning it was still
finding new states every ~10 minutes).

Some statistics
===============

LTL problems:
* Problem 1   50s     13 states     6 hypotheses
* Problem 2   1m22s   22 states     10 hypotheses
* Problem 3   7m05s   26 states     13 hypotheses
* Problem 4   34m     157 states    77 hypotheses
* Problem 5   2h43m   121 states    50 hypotheses
* Problem 6   4h51m   238 states    156 hypotheses
* Problem 7   11h45m  610 states    407 hypotheses
* Problem 8   24h22m  646 states    432 hypotheses
* Problem 9   18h31m  854 states    550 hypotheses

Reachability
* Problem 10  2m39s   59 states     3 hypotheses
* Problem 11  1w+     22589 states  8314 hypotheses
* Problem 12  1w+     12771 states  4325 hypotheses
* Problem 13  1w+     12848 states  5654 hypotheses
* Problem 14  1w+     11632 states  4513 hypotheses
* Problem 15  1w+     7821 states   3792 hypotheses
* Problem 16  1w+     8425 states   3865 hypotheses
* Problem 17  1w+     11758 states  5584 hypotheses
* Problem 18  1w+     8863 states   4246 hypotheses
