package learnlib;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import de.learnlib.acex.analyzers.AcexAnalyzers;
import de.learnlib.algorithms.ttt.mealy.TTTLearnerMealy;
import de.learnlib.api.EquivalenceOracle;
import de.learnlib.api.LearningAlgorithm;
import de.learnlib.api.MembershipOracle;
import de.learnlib.eqtests.basic.EQOracleChain;
import de.learnlib.eqtests.basic.SampleSetEQOracle;
import de.learnlib.oracles.DefaultQuery;
import de.learnlib.oracles.SULOracle;
import net.automatalib.automata.transout.MealyMachine;
import net.automatalib.util.graphs.dot.GraphDOT;
import net.automatalib.words.Word;
import net.automatalib.words.WordBuilder;

import java.io.*;
import java.nio.file.Path;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.*;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/**
 * Simple learning set up for the RERS challenge. Used as a baseline.
 * Created by joshua on 15/07/2016.
 */
public class RERSMain {
    // All options. JCommander automatically generates a nice help message (including the defaults) when running the program.
    public static class CommandLineOptions {
        @Parameter(description = "Some dot file")
        public List<String> filenames = new ArrayList<>(1);

        @Parameter(names = "-depth", description = "The depth used for fixed test methods (WMethod, LeeYannakakisFixes, ...). Used as minimum for randomized methods")
        public int maxDepth = 2;

        @Parameter(names = "-expectedLength", description = "The expected length of the middle part in randomized testing")
        public int expectedLength = 8;

        @Parameter(names = "-initialSuite", description = "Loads an initial test suite (useful for continuing experiments)")
        public Path initialSuite = null;

        @Parameter(names = "-reuseCE", description = "Should CEs always be retested? (This is not counted as extra queries&inputs)", arity = 1)
        public boolean reuseCE = true;

        @Parameter(names = "-useCache", description = "Enable cache", arity = 1)
        public boolean useCache = true;

        @Parameter(names = "-cacheBound", description = "The cache is memory-intensive, this bounds the size of the cache")
        public int cacheBound = 500000;

        @Parameter(names = "-alphabetSize", description = "Size of alphabet")
        public int alphabetSize = 20;

        @Parameter(names = "-saveAllHypotheses", description = "Flag to turn on hypotheses saving")
        public boolean saveAllHypotheses = false;

        @Parameter(names = { "--help", "-h" }, help = true)
        public boolean help;

        @Override
        public String toString() {
            return "CommandLineOptions{" +
                    "filenames=" + filenames +
                    ", maxDepth=" + maxDepth +
                    ", expectedLength=" + expectedLength +
                    ", initialSuite='" + initialSuite + '\'' +
                    ", reuseCE=" + reuseCE +
                    ", useCache=" + useCache +
                    ", cacheBound=" + cacheBound +
                    ", alphabetSize=" + alphabetSize +
                    ", saveAllHypotheses=" + saveAllHypotheses +
                    ", help=" + help +
                    '}';
        }
    }

    public static void main(String [] args) {
        try {
            innerMain(args);
        } catch (Exception e) {
            System.err.println("Exception " + Arrays.toString(args));
            e.printStackTrace();
            System.exit(1);
        }
    }

    public static void innerMain(String[] args) throws IOException {
        // First we parse the command line options
        CommandLineOptions opts = new CommandLineOptions();
        JCommander c = new JCommander(opts, args);
        if (opts.help) {
            c.usage();
            return;
        }

        // For backlog, print all used options
        System.out.println("Options: " + opts);

        // If there is no filename. I have a preset filename for easy running in my IDE
        // In normal usage, there is always a filename.
        if (opts.filenames.isEmpty() || opts.filenames.size() > 1) {
            System.err.println("ERROR> warning: no or too many files given. I'll use " + opts.filenames.get(0));
        }

        LocalDateTime start = LocalDateTime.now();
        System.out.println("LOG> start: " + start + ", alphabet = " + opts.alphabetSize);

        IntAlphabet alphabet = new IntAlphabet(opts.alphabetSize);
        ProcessSUL sul = new ProcessSUL(opts.filenames.get(0));
        MembershipOracle<Integer, Word<String>> membershipOracle = new SULOracle<>(sul);


        // We will count both the number of queries for testing and for learning. This is important, since
        // they might influence each other (shorter testing might lead to more learning, and vice versa).
        // In the end we are really interested in the combined result (but this is part of the data
        // post processing).
        CountingMembershipOracle<Integer, Word<String>> mCountingOracleForLearning = new CountingMembershipOracle<>(membershipOracle, "learning");
        CountingMembershipOracle<Integer, Word<String>> mCountingOracleForTesting = new CountingMembershipOracle<>(membershipOracle, "testing");
        MembershipOracle<Integer, Word<String>> mOracleForLearning = mCountingOracleForLearning;
        MembershipOracle<Integer, Word<String>> mOracleForTesting = mCountingOracleForTesting;


        // Optionally make use of a cache. Note that counting for learning and testing are still
        // distinguished, but there is a shared cache. Since the normal caching things crash all the time,
        // and since I want a bit more control, I implemented the CachedOracle myself.
        Cache<Integer, String> cache = null;
        if(opts.useCache) {
            try {
                System.out.println("Reading cache.");
                ObjectInputStream ois = new ObjectInputStream(new GZIPInputStream(new FileInputStream("traces.cache")));
                cache = (Cache<Integer, String>)ois.readObject();
                ois.close();
            } catch (IOException | ClassNotFoundException e) {
                System.out.println("Could not read cache: " + e + ". Creating new cache.");
                cache = new RERSCache();
            }
            System.out.println("Done with cache.");
            assert cache != null;
            mOracleForLearning = new CachedOracle<>(cache, mOracleForLearning, opts.cacheBound, true);
            mOracleForTesting = new CachedOracle<>(cache, mOracleForTesting, opts.cacheBound, false);
        }

        // We fix a decent testing method in this experiment.
        EquivalenceOracle<MealyMachine<?, Integer, ?, String>, Integer, Word<String>> eqOracle = new RandomWpMethod<>(mOracleForTesting, opts.maxDepth, opts.expectedLength);

        // Optionally we can globally (independent of the oracle) retry CEs
        SampleSetEQOracle<Integer, Word<String>> reusingOracle = null;
        if (opts.reuseCE) {
            // false means never to remove CEs
            reusingOracle = new SampleSetEQOracle<>(false);
            eqOracle = new EQOracleChain<>(reusingOracle, eqOracle);
        }

        // If a file is specified as initial suite, we will first test those
        if (opts.initialSuite != null) {
            System.out.println("Reading initial suite.");
            List<Word<Integer>> suite = new ArrayList<>();
            Scanner s = new Scanner(opts.initialSuite);
            while(s.hasNextLine()) {
                String line = s.nextLine();
                Scanner lineScanner = new Scanner(line);

                // divided by two because of spaces between characters
                WordBuilder<Integer> wb = new WordBuilder<>(line.length() / 2 + 1);

                // add each symbol to the word
                // TODO: sanitize input?
                while(lineScanner.hasNextInt()) {
                    wb = wb.append(lineScanner.nextInt());
                }

                suite.add(wb.toWord());
            }

            // why not first shuffle
            Collections.shuffle(suite);

            // true means to remove unsuccesful CEs
            SampleSetEQOracle<Integer, Word<String>> testSuiteEq = new SampleSetEQOracle<>(true);

            // Adds suite to the testing oracle. Note that this will run the SUL
            System.out.println("Executing initial suite.");
            testSuiteEq.addAll(mOracleForTesting, suite);

            // replace the oracle by first checking the suite
            eqOracle = new EQOracleChain<>(testSuiteEq, eqOracle);
            System.out.println("Done with initial suite.");
        }

        // Fix learner, just LStar as a baseline
        LearningAlgorithm<MealyMachine<?, Integer, ?, String>, Integer, Word<String>> learner = new TTTLearnerMealy<>(alphabet, mOracleForLearning, AcexAnalyzers.LINEAR_FWD);


        // Here we will perform our experiment. I did not use the Experiment class from LearnLib, as I wanted some
        // more control (for example, I want to reset the counters in the membership oracles). This control flow
        // is suggested by LearnLib (on their wiki).
        int stage = 0;
        learner.startLearning();

        while(true) {
            stage++;

            if(opts.saveAllHypotheses) {
                String dir = "./hypos/";
                String outputFilename = dir + "hyp." + stage + ".obf.dot";
                PrintWriter output = new PrintWriter(outputFilename);
                GraphDOT.write(learner.getHypothesisModel(), alphabet, output);
                output.close();
            }

            if(cache != null) {
                System.out.println("Cache has nodes: " + cache.size());
                ObjectOutputStream oos = new ObjectOutputStream(new GZIPOutputStream(new FileOutputStream(new File("traces.cache"))));
                oos.writeObject(cache);
                oos.close();
            }

            // Print statistics
            LocalDateTime now = LocalDateTime.now();
            System.out.println(stage + ": " + now + " duration so far: " + Duration.between(start, now));
            System.out.println("States: " + learner.getHypothesisModel().size());
            mCountingOracleForLearning.log(System.out, true);
            mCountingOracleForTesting.log(System.out, true);
            System.out.println();

            // Search for CE
            DefaultQuery<Integer, Word<String>> ce = eqOracle.findCounterExample(learner.getHypothesisModel(), alphabet);
            if(ce == null) break;

            // Add it to the set of known CEs
            if(reusingOracle != null) reusingOracle.addAll(ce);

            // Rinse and repeat
            learner.refineHypothesis(ce);
        }

        System.out.println("ERROR> Done with learning " + opts.filenames.get(0));
        System.exit(0);
    }
}
