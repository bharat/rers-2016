package learnlib;

import net.automatalib.words.abstractimpl.AbstractAlphabet;

/**
 * An alphabet implementation only using integers. There is a small gain for large alphabets,
 * as no hash has to be calculated to get an id for a symbol.
 * Created by joshua on 20/07/2016.
 */
public class IntAlphabet extends AbstractAlphabet<Integer> {
    private final int bound;

    public IntAlphabet(int bound) {
        this.bound = bound;
    }

    @Override
    public Integer getSymbol(int index) {
        return index;
    }

    @Override
    public int getSymbolIndex(Integer symbol) {
        return symbol;
    }

    @Override
    public int size() {
        return bound;
    }

    /* (non-Javadoc)
     * @see net.automatalib.words.abstractimpl.AbstractAlphabet#writeToArray(int, java.lang.Object[], int, int)
     */
    @Override
    public void writeToArray(int offset, Object[] array, int tgtOfs, int num) {
        for (int i = offset; i < num; i++) {
            array[i + tgtOfs] = i;
        }
    }

    @Override
    public boolean containsSymbol(Integer symbol) {
        return symbol >= 0 && symbol < bound;
    }

}
