package learnlib;

import net.automatalib.words.Word;
import net.automatalib.words.WordBuilder;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Objects;

/**
 * Simple Trie based cache, no fancyness. But serializable
 */
public class RERSCache implements Serializable, Cache<Integer, String> {
    private ArrayList<RERSCache> successors;
    private ArrayList<String> output;
    private int count = 0;

    public RERSCache() {
        successors = new ArrayList<>();
        output = new ArrayList<>();
    }

    @Override
    public int estimatedSize() {
        return size();
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public Word<String> lookup(Word<Integer> input) {
        WordBuilder<String> outputWord = new WordBuilder<>(input.length());
        boolean ret = lookupBuild(input, outputWord);
        if (ret) {
            return outputWord.toWord();
        } else {
            return null;
        }
    }

    private boolean lookupBuild(Word<Integer> input, WordBuilder<String> outputWord) {
        if (input.isEmpty()) {
            return true;
        }

        int x = input.firstSymbol();
        Word<Integer> next = input.subWord(1);

        assert x >= 0;
        if (x < output.size() && output.get(x) != null) {
            outputWord.append(output.get(x));
            return successors.get(x).lookupBuild(next, outputWord);
        }

        return false;
    }

    @Override
    public int insert(Word<Integer> input, Word<String> outputWord) {
        assert input.size() == outputWord.size();
        if (input.isEmpty()) {
            return 0;
        }

        int x = input.firstSymbol();
        Word<Integer> next = input.subWord(1);

        assert x >= 0;
        if (x < successors.size() && successors.get(x) != null) {
            assert Objects.equals(output.get(x), outputWord.firstSymbol());
            int added = successors.get(x).insert(next, outputWord.subWord(1));
            count += added;
            return added;
        }

        RERSCache subTrie = new RERSCache();

        // grow array if needed (ugly)
        successors.ensureCapacity(x);
        output.ensureCapacity(x);
        while(x >= successors.size()){
            successors.add(null);
            output.add(null);
        }

        successors.set(x, subTrie);
        output.set(x, outputWord.firstSymbol());
        int added = subTrie.insert(next, outputWord.subWord(1)) + 1;
        count += added;
        return added;
    }
}
