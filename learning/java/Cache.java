package learnlib;

import net.automatalib.words.Word;

/**
 * Created by joshua on 28/07/2016.
 */
public interface Cache<I, O> {
    // Some implementations have a very slow size calculation
    // so we add this for performance reasons...
    public int estimatedSize();

    public int size();

    public Word<O> lookup(Word<I> input);

    // Return number of nodes added, might be approximate
    public int insert(Word<I> input, Word<O> outputWord);
}
