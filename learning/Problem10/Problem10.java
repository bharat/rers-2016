import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Problem10 {
	static BufferedReader stdin = new BufferedReader(new InputStreamReader(System.in));

	private String[] inputs = {"B","E","C","A","D"};

	public int a82 = 6;
	public int a170 = 6;
	public String a16 = "f";
	public int a39 = 2;
	public String a65 = "g";
	public int a130 = 11;
	public String a14 = "f";
	public int a111 = 10;
	public String a63 = "f";
	public int a36 = 11;
	public int a182 = 13;
	public String a24 = "f";
	public int a7 = 5;
	public String a116 = "e";
	public int a41 = 2;
	public String a150 = "f";
	public int a37 = 11;
	public int a142 = 6;
	public int a121 = 16;
	public String a25 = "f";
	public String a123 = "g";
	public int a0 = 11;
	public int a90 = 6;
	public String a162 = "e";
	public String a104 = "i";
	public String a66 = "i";
	public String a84 = "g";
	public String a164 = "f";
	public String a197 = "f";
	public String a200 = "f";
	public String a30 = "f";
	public String a189 = "g";
	public String a86 = "f";
	public String a70 = "i";
	public int a155 = 13;
	public String a120 = "f";
	public int a114 = 9;
	public int a110 = 10;
	public String a131 = "f";
	public int a160 = 9;
	public String a127 = "g";
	public int a44 = 9;
	public int a117 = 17;
	public int a77 = 15;
	public int a54 = 8;
	public int a179 = 11;
	public int a6 = 11;
	public int a188 = 4;
	public String a69 = "g";
	public int a79 = 9;
	public String a29 = "h";
	public int a45 = 11;
	public String a51 = "f";
	public String a196 = "f";
	public int a15 = 11;
	public int a109 = 6;
	public int a91 = 8;
	public int a92 = 5;
	public int a67 = 10;
	public int a95 = 10;
	public int a129 = 14;
	public String a60 = "f";
	public int a195 = 11;
	public int a176 = 7;
	public int a23 = 4;
	public String a38 = "h";
	public int a154 = 11;
	public int a31 = 3;
	public int a56 = 10;
	public boolean cf = true;
	public String a191 = "f";
	public int a168 = 8;

	private void errorCheck() {
	    if((((a90 == 9) && (a160 == 8)) && (a56 == 10))){
	    	cf = false;
	    	Errors.__VERIFIER_error(0);
	    }
	    if((((a179 == 10) && (a160 == 2)) && (a56 == 10))){
	    	cf = false;
	    	Errors.__VERIFIER_error(1);
	    }
	    if((((a91 == 6) && (a69.equals("f"))) && (a56 == 7))){
	    	cf = false;
	    	Errors.__VERIFIER_error(2);
	    }
	    if((((a116.equals("h")) && (a160 == 3)) && (a56 == 10))){
	    	cf = false;
	    	Errors.__VERIFIER_error(3);
	    }
	    if((((a110 == 12) && (a109 == 7)) && (a56 == 12))){
	    	cf = false;
	    	Errors.__VERIFIER_error(4);
	    }
	    if((((a176 == 10) && (a160 == 4)) && (a56 == 10))){
	    	cf = false;
	    	Errors.__VERIFIER_error(5);
	    }
	    if((((a179 == 12) && (a60.equals("i"))) && (a56 == 9))){
	    	cf = false;
	    	Errors.__VERIFIER_error(6);
	    }
	    if((((a121 == 9) && (a69.equals("i"))) && (a56 == 7))){
	    	cf = false;
	    	Errors.__VERIFIER_error(7);
	    }
	    if((((a45 == 13) && (a109 == 2)) && (a56 == 12))){
	    	cf = false;
	    	Errors.__VERIFIER_error(8);
	    }
	    if((((a130 == 10) && (a109 == 8)) && (a56 == 12))){
	    	cf = false;
	    	Errors.__VERIFIER_error(9);
	    }
	    if((((a67 == 8) && (a69.equals("g"))) && (a56 == 7))){
	    	cf = false;
	    	Errors.__VERIFIER_error(10);
	    }
	    if((((a155 == 11) && (a60.equals("e"))) && (a56 == 9))){
	    	cf = false;
	    	Errors.__VERIFIER_error(11);
	    }
	    if((((a155 == 13) && (a60.equals("e"))) && (a56 == 9))){
	    	cf = false;
	    	Errors.__VERIFIER_error(12);
	    }
	    if((((a176 == 7) && (a69.equals("h"))) && (a56 == 7))){
	    	cf = false;
	    	Errors.__VERIFIER_error(13);
	    }
	    if((((a31 == 4) && (a104.equals("f"))) && (a56 == 13))){
	    	cf = false;
	    	Errors.__VERIFIER_error(14);
	    }
	    if((((a29.equals("f")) && (a160 == 4)) && (a56 == 8))){
	    	cf = false;
	    	Errors.__VERIFIER_error(15);
	    }
	    if((((a31 == 3) && (a70.equals("g"))) && (a56 == 11))){
	    	cf = false;
	    	Errors.__VERIFIER_error(16);
	    }
	    if((((a179 == 11) && (a60.equals("i"))) && (a56 == 9))){
	    	cf = false;
	    	Errors.__VERIFIER_error(17);
	    }
	    if((((a31 == 6) && (a104.equals("f"))) && (a56 == 13))){
	    	cf = false;
	    	Errors.__VERIFIER_error(18);
	    }
	    if((((a195 == 8) && (a104.equals("g"))) && (a56 == 13))){
	    	cf = false;
	    	Errors.__VERIFIER_error(19);
	    }
	    if((((a160 == 4) && (a109 == 3)) && (a56 == 12))){
	    	cf = false;
	    	Errors.__VERIFIER_error(20);
	    }
	    if((((a154 == 7) && (a38.equals("e"))) && (a56 == 14))){
	    	cf = false;
	    	Errors.__VERIFIER_error(21);
	    }
	    if((((a110 == 8) && (a109 == 7)) && (a56 == 12))){
	    	cf = false;
	    	Errors.__VERIFIER_error(22);
	    }
	    if((((a90 == 5) && (a160 == 8)) && (a56 == 10))){
	    	cf = false;
	    	Errors.__VERIFIER_error(23);
	    }
	    if((((a189.equals("i")) && (a70.equals("e"))) && (a56 == 11))){
	    	cf = false;
	    	Errors.__VERIFIER_error(24);
	    }
	    if((((a31 == 4) && (a70.equals("g"))) && (a56 == 11))){
	    	cf = false;
	    	Errors.__VERIFIER_error(25);
	    }
	    if((((a162.equals("i")) && (a104.equals("e"))) && (a56 == 13))){
	    	cf = false;
	    	Errors.__VERIFIER_error(26);
	    }
	    if((((a155 == 7) && (a60.equals("e"))) && (a56 == 9))){
	    	cf = false;
	    	Errors.__VERIFIER_error(27);
	    }
	    if((((a182 == 12) && (a60.equals("g"))) && (a56 == 9))){
	    	cf = false;
	    	Errors.__VERIFIER_error(28);
	    }
	    if((((a45 == 16) && (a109 == 2)) && (a56 == 12))){
	    	cf = false;
	    	Errors.__VERIFIER_error(29);
	    }
	    if((((a110 == 11) && (a109 == 7)) && (a56 == 12))){
	    	cf = false;
	    	Errors.__VERIFIER_error(30);
	    }
	    if((((a69.equals("h")) && (a160 == 9)) && (a56 == 8))){
	    	cf = false;
	    	Errors.__VERIFIER_error(31);
	    }
	    if((((a65.equals("e")) && (a109 == 4)) && (a56 == 12))){
	    	cf = false;
	    	Errors.__VERIFIER_error(32);
	    }
	    if((((a110 == 10) && (a109 == 7)) && (a56 == 12))){
	    	cf = false;
	    	Errors.__VERIFIER_error(33);
	    }
	    if((((a51.equals("g")) && (a109 == 6)) && (a56 == 12))){
	    	cf = false;
	    	Errors.__VERIFIER_error(34);
	    }
	    if((((a160 == 5) && (a109 == 3)) && (a56 == 12))){
	    	cf = false;
	    	Errors.__VERIFIER_error(35);
	    }
	    if((((a90 == 11) && (a160 == 7)) && (a56 == 10))){
	    	cf = false;
	    	Errors.__VERIFIER_error(36);
	    }
	    if((((a179 == 16) && (a60.equals("i"))) && (a56 == 9))){
	    	cf = false;
	    	Errors.__VERIFIER_error(37);
	    }
	    if((((a123.equals("i")) && (a160 == 5)) && (a56 == 8))){
	    	cf = false;
	    	Errors.__VERIFIER_error(38);
	    }
	    if((((a130 == 12) && (a109 == 8)) && (a56 == 12))){
	    	cf = false;
	    	Errors.__VERIFIER_error(39);
	    }
	    if((((a90 == 10) && (a160 == 7)) && (a56 == 10))){
	    	cf = false;
	    	Errors.__VERIFIER_error(40);
	    }
	    if((((a176 == 6) && (a160 == 4)) && (a56 == 10))){
	    	cf = false;
	    	Errors.__VERIFIER_error(41);
	    }
	    if((((a129 == 15) && (a160 == 6)) && (a56 == 8))){
	    	cf = false;
	    	Errors.__VERIFIER_error(42);
	    }
	    if((((a23 == 7) && (a69.equals("e"))) && (a56 == 7))){
	    	cf = false;
	    	Errors.__VERIFIER_error(43);
	    }
	    if((((a130 == 15) && (a109 == 8)) && (a56 == 12))){
	    	cf = false;
	    	Errors.__VERIFIER_error(44);
	    }
	    if((((a116.equals("f")) && (a160 == 3)) && (a56 == 10))){
	    	cf = false;
	    	Errors.__VERIFIER_error(45);
	    }
	    if((((a45 == 14) && (a109 == 2)) && (a56 == 12))){
	    	cf = false;
	    	Errors.__VERIFIER_error(46);
	    }
	    if((((a66.equals("e")) && (a60.equals("f"))) && (a56 == 9))){
	    	cf = false;
	    	Errors.__VERIFIER_error(47);
	    }
	    if((((a182 == 15) && (a60.equals("g"))) && (a56 == 9))){
	    	cf = false;
	    	Errors.__VERIFIER_error(48);
	    }
	    if((((a117 == 17) && (a38.equals("i"))) && (a56 == 14))){
	    	cf = false;
	    	Errors.__VERIFIER_error(49);
	    }
	    if((((a66.equals("g")) && (a60.equals("f"))) && (a56 == 9))){
	    	cf = false;
	    	Errors.__VERIFIER_error(50);
	    }
	    if((((a23 == 2) && (a160 == 9)) && (a56 == 10))){
	    	cf = false;
	    	Errors.__VERIFIER_error(51);
	    }
	    if((((a189.equals("f")) && (a70.equals("e"))) && (a56 == 11))){
	    	cf = false;
	    	Errors.__VERIFIER_error(52);
	    }
	    if((((a67 == 10) && (a69.equals("g"))) && (a56 == 7))){
	    	cf = false;
	    	Errors.__VERIFIER_error(53);
	    }
	    if((((a182 == 10) && (a60.equals("g"))) && (a56 == 9))){
	    	cf = false;
	    	Errors.__VERIFIER_error(54);
	    }
	    if((((a179 == 13) && (a160 == 2)) && (a56 == 10))){
	    	cf = false;
	    	Errors.__VERIFIER_error(55);
	    }
	    if((((a65.equals("f")) && (a109 == 4)) && (a56 == 12))){
	    	cf = false;
	    	Errors.__VERIFIER_error(56);
	    }
	    if((((a179 == 14) && (a60.equals("i"))) && (a56 == 9))){
	    	cf = false;
	    	Errors.__VERIFIER_error(57);
	    }
	    if((((a176 == 8) && (a69.equals("h"))) && (a56 == 7))){
	    	cf = false;
	    	Errors.__VERIFIER_error(58);
	    }
	    if((((a127.equals("e")) && (a38.equals("h"))) && (a56 == 14))){
	    	cf = false;
	    	Errors.__VERIFIER_error(59);
	    }
	    if((((a176 == 3) && (a69.equals("h"))) && (a56 == 7))){
	    	cf = false;
	    	Errors.__VERIFIER_error(60);
	    }
	    if((((a176 == 10) && (a69.equals("h"))) && (a56 == 7))){
	    	cf = false;
	    	Errors.__VERIFIER_error(61);
	    }
	    if((((a44 == 9) && (a160 == 7)) && (a56 == 8))){
	    	cf = false;
	    	Errors.__VERIFIER_error(62);
	    }
	    if((((a23 == 4) && (a109 == 9)) && (a56 == 12))){
	    	cf = false;
	    	Errors.__VERIFIER_error(63);
	    }
	    if((((a154 == 8) && (a38.equals("e"))) && (a56 == 14))){
	    	cf = false;
	    	Errors.__VERIFIER_error(64);
	    }
	    if((((a54 == 5) && (a160 == 8)) && (a56 == 8))){
	    	cf = false;
	    	Errors.__VERIFIER_error(65);
	    }
	    if((((a6 == 6) && (a38.equals("g"))) && (a56 == 14))){
	    	cf = false;
	    	Errors.__VERIFIER_error(66);
	    }
	    if((((a69.equals("e")) && (a160 == 9)) && (a56 == 8))){
	    	cf = false;
	    	Errors.__VERIFIER_error(67);
	    }
	    if((((a121 == 16) && (a69.equals("i"))) && (a56 == 7))){
	    	cf = false;
	    	Errors.__VERIFIER_error(68);
	    }
	    if((((a176 == 6) && (a69.equals("h"))) && (a56 == 7))){
	    	cf = false;
	    	Errors.__VERIFIER_error(69);
	    }
	    if((((a77 == 8) && (a60.equals("h"))) && (a56 == 9))){
	    	cf = false;
	    	Errors.__VERIFIER_error(70);
	    }
	    if((((a130 == 17) && (a160 == 3)) && (a56 == 8))){
	    	cf = false;
	    	Errors.__VERIFIER_error(71);
	    }
	    if((((a176 == 5) && (a160 == 4)) && (a56 == 10))){
	    	cf = false;
	    	Errors.__VERIFIER_error(72);
	    }
	    if((((a176 == 4) && (a160 == 4)) && (a56 == 10))){
	    	cf = false;
	    	Errors.__VERIFIER_error(73);
	    }
	    if((((a36 == 11) && (a160 == 2)) && (a56 == 8))){
	    	cf = false;
	    	Errors.__VERIFIER_error(74);
	    }
	    if((((a77 == 14) && (a60.equals("h"))) && (a56 == 9))){
	    	cf = false;
	    	Errors.__VERIFIER_error(75);
	    }
	    if((((a31 == 8) && (a104.equals("f"))) && (a56 == 13))){
	    	cf = false;
	    	Errors.__VERIFIER_error(76);
	    }
	    if((((a65.equals("i")) && (a109 == 4)) && (a56 == 12))){
	    	cf = false;
	    	Errors.__VERIFIER_error(77);
	    }
	    if((((a129 == 14) && (a160 == 6)) && (a56 == 8))){
	    	cf = false;
	    	Errors.__VERIFIER_error(78);
	    }
	    if((((a36 == 7) && (a160 == 2)) && (a56 == 8))){
	    	cf = false;
	    	Errors.__VERIFIER_error(79);
	    }
	    if((((a23 == 6) && (a109 == 9)) && (a56 == 12))){
	    	cf = false;
	    	Errors.__VERIFIER_error(80);
	    }
	    if((((a67 == 13) && (a69.equals("g"))) && (a56 == 7))){
	    	cf = false;
	    	Errors.__VERIFIER_error(81);
	    }
	    if((((a29.equals("g")) && (a160 == 4)) && (a56 == 8))){
	    	cf = false;
	    	Errors.__VERIFIER_error(82);
	    }
	    if((((a130 == 15) && (a160 == 3)) && (a56 == 8))){
	    	cf = false;
	    	Errors.__VERIFIER_error(83);
	    }
	    if((((a69.equals("g")) && (a160 == 9)) && (a56 == 8))){
	    	cf = false;
	    	Errors.__VERIFIER_error(84);
	    }
	    if((((a6 == 7) && (a38.equals("g"))) && (a56 == 14))){
	    	cf = false;
	    	Errors.__VERIFIER_error(85);
	    }
	    if((((a44 == 6) && (a160 == 7)) && (a56 == 8))){
	    	cf = false;
	    	Errors.__VERIFIER_error(86);
	    }
	    if((((a6 == 8) && (a38.equals("g"))) && (a56 == 14))){
	    	cf = false;
	    	Errors.__VERIFIER_error(87);
	    }
	    if((((a6 == 11) && (a38.equals("g"))) && (a56 == 14))){
	    	cf = false;
	    	Errors.__VERIFIER_error(88);
	    }
	    if((((a44 == 10) && (a160 == 7)) && (a56 == 8))){
	    	cf = false;
	    	Errors.__VERIFIER_error(89);
	    }
	    if((((a114 == 12) && (a38.equals("f"))) && (a56 == 14))){
	    	cf = false;
	    	Errors.__VERIFIER_error(90);
	    }
	    if((((a162.equals("f")) && (a104.equals("e"))) && (a56 == 13))){
	    	cf = false;
	    	Errors.__VERIFIER_error(91);
	    }
	    if((((a67 == 15) && (a69.equals("g"))) && (a56 == 7))){
	    	cf = false;
	    	Errors.__VERIFIER_error(92);
	    }
	    if((((a67 == 11) && (a69.equals("g"))) && (a56 == 7))){
	    	cf = false;
	    	Errors.__VERIFIER_error(93);
	    }
	    if((((a84.equals("g")) && (a109 == 5)) && (a56 == 12))){
	    	cf = false;
	    	Errors.__VERIFIER_error(94);
	    }
	    if((((a182 == 17) && (a60.equals("g"))) && (a56 == 9))){
	    	cf = false;
	    	Errors.__VERIFIER_error(95);
	    }
	    if((((a23 == 2) && (a69.equals("e"))) && (a56 == 7))){
	    	cf = false;
	    	Errors.__VERIFIER_error(96);
	    }
	    if((((a54 == 8) && (a160 == 8)) && (a56 == 8))){
	    	cf = false;
	    	Errors.__VERIFIER_error(97);
	    }
	    if((((a154 == 6) && (a38.equals("e"))) && (a56 == 14))){
	    	cf = false;
	    	Errors.__VERIFIER_error(98);
	    }
	    if((((a160 == 7) && (a109 == 3)) && (a56 == 12))){
	    	cf = false;
	    	Errors.__VERIFIER_error(99);
	    }
	}private  void calculateOutputm46(String input) {
    if((((a95 == 9) && (a39 == 1)) && ((a82 == 5) && ((((cf && (input.equals("C"))) && (a15 == 10)) && (a150.equals("e"))) && (a150.equals("e")))))) {
    	cf = false;
    	a39 = 2;
    	a41 = 2;
    	a24 = "f";
    	a168 = 8;
    	a82 = 6;
    	a38 = "e";
    	a63 = "f";
    	a56 = 14;
    	a95 = 10;
    	a7 = 5;
    	a79 = 9;
    	a154 = 6; 
    	System.out.println("Y");
    } 
    if((((a168 == 7) && ((((input.equals("D")) && cf) && (a92 == 4)) && (a63.equals("e")))) && ((a0 == 10) && ((a164.equals("e")) && (a41 == 1))))) {
    	cf = false;
    	a200 = "f";
    	a56 = 12;
    	a82 = 6;
    	a95 = 10;
    	a197 = "g";
    	a131 = "e";
    	a63 = "f";
    	a188 = 5;
    	a24 = "g";
    	a196 = "f";
    	a150 = "f";
    	a39 = 2;
    	a14 = "f";
    	a191 = "f";
    	a168 = 8;
    	a79 = 9;
    	a111 = 10;
    	a109 = 7;
    	a37 = 11;
    	a7 = 5;
    	a41 = 2;
    	a164 = "f";
    	a92 = 5;
    	a0 = 11;
    	a25 = "f";
    	a110 = 13; 
    	System.out.println("Y");
    } 
    if((((a7 == 4) && (((input.equals("A")) && cf) && (a197.equals("e")))) && ((a24.equals("e")) && ((a25.equals("e")) && ((a82 == 5) && (a200.equals("e"))))))) {
    	cf = false;
    	a120 = "e";
    	a69 = "g";
    	a131 = "e";
    	a67 = 10; 
    	System.out.println("Y");
    } 
    if((((a191.equals("e")) && (a7 == 4)) && ((a111 == 9) && ((((a86.equals("e")) && ((input.equals("B")) && cf)) && (a14.equals("e"))) && (a15 == 10))))) {
    	cf = false;
    	a197 = "f";
    	a150 = "f";
    	a70 = "g";
    	a164 = "f";
    	a14 = "f";
    	a56 = 11;
    	a7 = 5;
    	a86 = "f";
    	a95 = 10;
    	a31 = 3; 
    	System.out.println("Y");
    } 
    if((((a15 == 10) && ((a41 == 1) && (((input.equals("E")) && cf) && (a37 == 10)))) && ((a111 == 9) && ((a41 == 1) && (a25.equals("e")))))) {
    	cf = false;
    	a191 = "f";
    	a86 = "f";
    	a63 = "f";
    	a79 = 9;
    	a189 = "i";
    	a150 = "f";
    	a39 = 2;
    	a14 = "f";
    	a197 = "f";
    	a92 = 5;
    	a7 = 5;
    	a164 = "f";
    	a70 = "e";
    	a56 = 11; 
    	System.out.println("Y");
    } 
}
private  void calculateOutputm2(String input) {
    if(((((a200.equals("e")) && ((a91 == 10) && cf)) && (a197.equals("e"))) && (((a41 == 1) && ((a95 == 9) && (a24.equals("e")))) && (a150.equals("e"))))) {
    	calculateOutputm46(input);
    } 
}
private  void calculateOutputm60(String input) {
    if(((((input.equals("B")) && cf) && (a196.equals("f"))) && (((a7 == 5) && ((a37 == 11) && ((a0 == 11) && (a197.equals("f"))))) && (a188 == 4)))) {
    	cf = false;
    	a109 = 7;
    	a56 = 12;
    	a110 = 11; 
    	System.out.println("Z");
    } 
    if((((a25.equals("f")) && ((a191.equals("f")) && ((a41 == 2) && (((input.equals("E")) && cf) && (a150.equals("f")))))) && ((a92 == 5) && (a111 == 10)))) {
    	cf = false;
    	a56 = 10;
    	a86 = "g";
    	a160 = 4;
    	a176 = 9; 
    	System.out.println("Y");
    } 
    if(((((a200.equals("f")) && ((cf && (input.equals("A"))) && (a142 == 6))) && (a196.equals("f"))) && ((a86.equals("f")) && ((a196.equals("f")) && (a86.equals("f")))))) {
    	cf = false;
    	a56 = 14;
    	a38 = "f";
    	a114 = 12; 
    	System.out.println("Y");
    } 
    if((((a120.equals("f")) && ((a168 == 8) && ((a15 == 11) && (a111 == 10)))) && ((a82 == 6) && ((cf && (input.equals("D"))) && (a92 == 5))))) {
    	cf = false;
    	a160 = 6;
    	a129 = 14; 
    	System.out.println("Y");
    } 
    if(((((cf && (input.equals("C"))) && (a150.equals("f"))) && (a63.equals("f"))) && ((a170 == 6) && (((a120.equals("f")) && (a39 == 2)) && (a111 == 10))))) {
    	cf = false;
    	a160 = 7;
    	a44 = 6; 
    	System.out.println("Y");
    } 
}
private  void calculateOutputm6(String input) {
    if((((a197.equals("f")) && (a24.equals("f"))) && ((a131.equals("f")) && (((a41 == 2) && ((a92 == 5) && ((a36 == 10) && cf))) && (a30.equals("f")))))) {
    	calculateOutputm60(input);
    } 
}
private  void calculateOutputm67(String input) {
    if((((a170 == 6) && (((a37 == 11) && (a7 == 5)) && (a7 == 5))) && (((a95 == 10) && (cf && (input.equals("C")))) && (a39 == 2)))) {
    	cf = false;
    	a92 = 5;
    	a129 = 12; 
    	System.out.println("Z");
    } 
    if((((a196.equals("f")) && ((cf && (input.equals("D"))) && (a41 == 2))) && ((((a86.equals("f")) && (a111 == 10)) && (a24.equals("f"))) && (a164.equals("f"))))) {
    	cf = false;
    	a56 = 12;
    	a109 = 3;
    	a160 = 5; 
    	System.out.println("Y");
    } 
    if((((a196.equals("f")) && (a111 == 10)) && ((a0 == 11) && ((a150.equals("f")) && (((a30.equals("f")) && (cf && (input.equals("A")))) && (a82 == 6)))))) {
    	cf = false;
    	a69 = "e";
    	a24 = "e";
    	a191 = "e";
    	a197 = "e";
    	a86 = "e";
    	a63 = "e";
    	a170 = 5;
    	a150 = "e";
    	a0 = 10;
    	a79 = 8;
    	a168 = 7;
    	a200 = "e";
    	a56 = 7;
    	a39 = 1;
    	a188 = 3;
    	a7 = 4;
    	a23 = 7; 
    	System.out.println("Y");
    } 
    if(((((a196.equals("f")) && (((a170 == 6) && ((a0 == 11) && (a200.equals("f")))) && (a82 == 6))) && (a120.equals("f"))) && ((input.equals("B")) && cf))) {
    	cf = false;
    	a129 = 15; 
    	System.out.println("Y");
    } 
    if(((((a7 == 5) && (a191.equals("f"))) && (a39 == 2)) && ((a95 == 10) && ((((input.equals("E")) && cf) && (a188 == 4)) && (a164.equals("f")))))) {
    	cf = false;
    	a63 = "g";
    	a188 = 5;
    	a60 = "h";
    	a111 = 11;
    	a95 = 11;
    	a196 = "g";
    	a142 = 7;
    	a16 = "g";
    	a37 = 12;
    	a56 = 9;
    	a170 = 7;
    	a150 = "g";
    	a24 = "g";
    	a200 = "g";
    	a41 = 3;
    	a7 = 6;
    	a0 = 12;
    	a25 = "g";
    	a77 = 14; 
    	System.out.println("Z");
    } 
}
private  void calculateOutputm68(String input) {
    if(((((a142 == 6) && (((input.equals("D")) && cf) && (a39 == 2))) && (a168 == 8)) && ((a0 == 11) && ((a131.equals("f")) && (a63.equals("f")))))) {
    	cf = false;
    	a160 = 7;
    	a131 = "f";
    	a63 = "f";
    	a44 = 4; 
    	System.out.println("W");
    } 
    if(((((a150.equals("f")) && ((a197.equals("f")) && (a7 == 5))) && (a39 == 2)) && (((a131.equals("f")) && ((input.equals("C")) && cf)) && (a164.equals("f"))))) {
    	cf = false;
    	a95 = 11;
    	a63 = "g";
    	a41 = 3;
    	a197 = "g";
    	a7 = 6;
    	a14 = "g";
    	a200 = "g";
    	a60 = "h";
    	a196 = "g";
    	a56 = 9;
    	a170 = 7;
    	a16 = "g";
    	a39 = 3;
    	a168 = 9;
    	a142 = 7;
    	a30 = "g";
    	a86 = "g";
    	a164 = "g";
    	a150 = "g";
    	a111 = 11;
    	a92 = 6;
    	a25 = "g";
    	a15 = 12;
    	a24 = "g";
    	a82 = 7;
    	a120 = "g";
    	a0 = 12;
    	a191 = "g";
    	a77 = 13; 
    	System.out.println("X");
    } 
    if((((a95 == 10) && (((a92 == 5) && (a150.equals("f"))) && (a15 == 11))) && ((a92 == 5) && ((a131.equals("f")) && (cf && (input.equals("E"))))))) {
    	cf = false;
    	a109 = 3;
    	a56 = 12;
    	a160 = 7; 
    	System.out.println("Y");
    } 
    if(((((a131.equals("f")) && (a95 == 10)) && (a82 == 6)) && ((a79 == 9) && ((a7 == 5) && ((a191.equals("f")) && ((input.equals("A")) && cf)))))) {
    	cf = false;
    	a104 = "e";
    	a162 = "i";
    	a56 = 13; 
    	System.out.println("X");
    } 
    if((((((a41 == 2) && (a200.equals("f"))) && (a41 == 2)) && (a164.equals("f"))) && (((a63.equals("f")) && (cf && (input.equals("B")))) && (a63.equals("f"))))) {
    	cf = false;
    	a160 = 2;
    	a56 = 10;
    	a179 = 13; 
    	System.out.println("W");
    } 
}
private  void calculateOutputm10(String input) {
    if((((a25.equals("f")) && ((a129 == 11) && cf)) && (((a111 == 10) && (((a191.equals("f")) && (a79 == 9)) && (a79 == 9))) && (a111 == 10)))) {
    	calculateOutputm67(input);
    } 
    if(((((a63.equals("f")) && ((a142 == 6) && (a131.equals("f")))) && (a95 == 10)) && ((a7 == 5) && (((a129 == 12) && cf) && (a188 == 4))))) {
    	calculateOutputm68(input);
    } 
}
private  void calculateOutputm71(String input) {
    if((((a111 == 10) && ((a170 == 6) && (a188 == 4))) && ((((cf && (input.equals("D"))) && (a30.equals("f"))) && (a164.equals("f"))) && (a120.equals("f"))))) {
    	cf = false;
    	a92 = 6;
    	a160 = 6;
    	a129 = 11; 
    	System.out.println("Y");
    } 
    if((((a14.equals("f")) && ((a196.equals("f")) && (a41 == 2))) && (((a79 == 9) && (((input.equals("C")) && cf) && (a15 == 11))) && (a188 == 4)))) {
    	cf = false;
    	a92 = 6;
    	a160 = 6;
    	a129 = 11; 
    	System.out.println("Y");
    } 
    if(((((a200.equals("f")) && (cf && (input.equals("E")))) && (a95 == 10)) && ((((a37 == 11) && (a95 == 10)) && (a25.equals("f"))) && (a120.equals("f"))))) {
    	cf = false;
    	a160 = 6;
    	a92 = 6;
    	a129 = 11; 
    	System.out.println("Y");
    } 
    if((((((cf && (input.equals("B"))) && (a25.equals("f"))) && (a142 == 6)) && (a168 == 8)) && ((a197.equals("f")) && ((a200.equals("f")) && (a14.equals("f")))))) {
    	cf = false;
    	a162 = "f";
    	a104 = "e";
    	a56 = 13; 
    	System.out.println("Y");
    } 
    if((((cf && (input.equals("A"))) && (a0 == 11)) && (((((a39 == 2) && (a82 == 6)) && (a164.equals("f"))) && (a168 == 8)) && (a37 == 11)))) {
    	cf = false;
    	a123 = "i";
    	a160 = 5; 
    	System.out.println("W");
    } 
}
private  void calculateOutputm11(String input) {
    if(((((cf && (a44 == 4)) && (a14.equals("f"))) && (a25.equals("f"))) && (((a7 == 5) && ((a92 == 5) && (a7 == 5))) && (a16.equals("f"))))) {
    	calculateOutputm71(input);
    } 
}
private  void calculateOutputm90(String input) {
    if(((((a16.equals("g")) && (((input.equals("C")) && cf) && (a95 == 11))) && (a197.equals("g"))) && ((a164.equals("g")) && ((a170 == 7) && (a39 == 3))))) {
    	cf = false;
    	a189 = "f";
    	a7 = 5;
    	a197 = "f";
    	a170 = 6;
    	a86 = "f";
    	a70 = "e";
    	a25 = "f";
    	a30 = "f";
    	a14 = "f";
    	a39 = 2;
    	a200 = "f";
    	a164 = "f";
    	a150 = "f";
    	a56 = 11; 
    	System.out.println("X");
    } 
    if((((a14.equals("g")) && (((cf && (input.equals("D"))) && (a41 == 3)) && (a30.equals("g")))) && ((a196.equals("g")) && ((a142 == 7) && (a142 == 7))))) {
    	cf = false;
    	a60 = "i";
    	a168 = 7;
    	a37 = 12;
    	a131 = "g";
    	a188 = 5;
    	a79 = 8;
    	a179 = 13; 
    	System.out.println("U");
    } 
    if((((a16.equals("g")) && (a0 == 12)) && ((a150.equals("g")) && (((a63.equals("g")) && ((a168 == 9) && (cf && (input.equals("A"))))) && (a95 == 11))))) {
    	cf = false;
    	a56 = 8;
    	a7 = 5;
    	a150 = "f";
    	a63 = "f";
    	a14 = "f";
    	a30 = "f";
    	a170 = 6;
    	a41 = 2;
    	a69 = "h";
    	a95 = 10;
    	a86 = "f";
    	a142 = 6;
    	a25 = "f";
    	a16 = "f";
    	a160 = 9; 
    	System.out.println("W");
    } 
    if((((a15 == 12) && ((a41 == 3) && ((a111 == 11) && (a24.equals("g"))))) && (((cf && (input.equals("B"))) && (a30.equals("g"))) && (a86.equals("g"))))) {
    	cf = false;
    	a60 = "i";
    	a179 = 16; 
    	System.out.println("Y");
    } 
    if((((a25.equals("g")) && ((a15 == 12) && (a164.equals("g")))) && ((a200.equals("g")) && ((a191.equals("g")) && ((a142 == 7) && ((input.equals("E")) && cf)))))) {
    	cf = false;
    	a200 = "f";
    	a70 = "g";
    	a197 = "f";
    	a7 = 5;
    	a30 = "f";
    	a56 = 11;
    	a14 = "f";
    	a86 = "f";
    	a150 = "f";
    	a63 = "f";
    	a191 = "f";
    	a164 = "f";
    	a31 = 4; 
    	System.out.println("Z");
    } 
}
private  void calculateOutputm17(String input) {
    if((((a25.equals("g")) && ((a164.equals("g")) && ((a150.equals("g")) && ((cf && (a77 == 13)) && (a63.equals("g")))))) && ((a170 == 7) && (a15 == 12)))) {
    	calculateOutputm90(input);
    } 
}
private  void calculateOutputm94(String input) {
    if((((((a191.equals("g")) && ((input.equals("A")) && cf)) && (a150.equals("g"))) && (a86.equals("g"))) && ((a92 == 6) && ((a120.equals("g")) && (a39 == 3))))) {
    	cf = false;
    	a168 = 8;
    	a170 = 6;
    	a25 = "f";
    	a95 = 10;
    	a24 = "f";
    	a14 = "f";
    	a0 = 11;
    	a196 = "f";
    	a7 = 5;
    	a164 = "f";
    	a63 = "f";
    	a39 = 2;
    	a150 = "f";
    	a15 = 11;
    	a41 = 2;
    	a191 = "f";
    	a197 = "f";
    	a30 = "f";
    	a160 = 7;
    	a56 = 8;
    	a142 = 6;
    	a79 = 9;
    	a200 = "f";
    	a188 = 4;
    	a82 = 6;
    	a120 = "f";
    	a37 = 11;
    	a92 = 5;
    	a111 = 10;
    	a86 = "f";
    	a131 = "f";
    	a16 = "f";
    	a44 = 4; 
    	System.out.println("W");
    } 
    if(((((a25.equals("g")) && (a197.equals("g"))) && (a15 == 12)) && ((a0 == 12) && ((a131.equals("g")) && ((cf && (input.equals("D"))) && (a14.equals("g"))))))) {
    	cf = false;
    	a188 = 4;
    	a63 = "f";
    	a131 = "f";
    	a197 = "f";
    	a164 = "f";
    	a95 = 10;
    	a142 = 6;
    	a170 = 6;
    	a92 = 5;
    	a150 = "f";
    	a24 = "f";
    	a14 = "f";
    	a79 = 9;
    	a191 = "f";
    	a39 = 2;
    	a86 = "f";
    	a111 = 10;
    	a7 = 5;
    	a82 = 6;
    	a15 = 11;
    	a41 = 2;
    	a168 = 8;
    	a200 = "f";
    	a196 = "f";
    	a160 = 7;
    	a0 = 11;
    	a120 = "f";
    	a56 = 8;
    	a30 = "f";
    	a25 = "f";
    	a16 = "f";
    	a37 = 11;
    	a44 = 4; 
    	System.out.println("W");
    } 
    if((((a7 == 6) && ((a196.equals("g")) && ((((input.equals("C")) && cf) && (a197.equals("g"))) && (a7 == 6)))) && ((a86.equals("g")) && (a30.equals("g"))))) {
    	cf = false;
    	a63 = "e";
    	a150 = "e";
    	a15 = 10;
    	a39 = 1;
    	a7 = 4;
    	a95 = 9;
    	a197 = "e";
    	a0 = 10;
    	a86 = "e";
    	a56 = 7;
    	a120 = "e";
    	a82 = 5;
    	a69 = "h";
    	a176 = 10; 
    	System.out.println("Y");
    } 
    if((((a95 == 11) && ((a82 == 7) && ((a142 == 7) && (a82 == 7)))) && (((a95 == 11) && ((input.equals("E")) && cf)) && (a95 == 11)))) {
    	cf = false;
    	a7 = 5;
    	a82 = 6;
    	a86 = "f";
    	a111 = 10;
    	a56 = 8;
    	a188 = 4;
    	a142 = 6;
    	a160 = 8;
    	a24 = "f";
    	a25 = "f";
    	a150 = "f";
    	a164 = "f";
    	a196 = "f";
    	a39 = 2;
    	a79 = 9;
    	a54 = 8; 
    	System.out.println("Y");
    } 
    if((((a188 == 5) && ((a25.equals("g")) && (a37 == 12))) && ((a16.equals("g")) && (((a200.equals("g")) && ((input.equals("B")) && cf)) && (a82 == 7))))) {
    	cf = false;
    	a0 = 10;
    	a69 = "g";
    	a7 = 4;
    	a15 = 10;
    	a86 = "e";
    	a197 = "e";
    	a131 = "e";
    	a37 = 10;
    	a63 = "e";
    	a120 = "e";
    	a95 = 9;
    	a150 = "e";
    	a24 = "e";
    	a56 = 7;
    	a67 = 11; 
    	System.out.println("W");
    } 
}
private  void calculateOutputm18(String input) {
    if((((cf && (a179 == 13)) && (a188 == 5)) && (((((a16.equals("g")) && (a111 == 11)) && (a170 == 7)) && (a14.equals("g"))) && (a37 == 12)))) {
    	calculateOutputm94(input);
    } 
}
private  void calculateOutputm99(String input) {
    if(((((((a92 == 5) && (a37 == 11)) && (a142 == 6)) && (a168 == 8)) && (a41 == 2)) && ((a170 == 6) && (cf && (input.equals("C")))))) {
    	cf = false;
    	a109 = 8;
    	a56 = 12;
    	a197 = "g";
    	a164 = "e";
    	a39 = 2;
    	a130 = 13; 
    	System.out.println("W");
    } 
    if(((((input.equals("B")) && cf) && (a200.equals("f"))) && ((a7 == 5) && ((a188 == 4) && (((a168 == 8) && (a142 == 6)) && (a15 == 11)))))) {
    	cf = false;
    	a109 = 8;
    	a56 = 12;
    	a130 = 12; 
    	System.out.println("W");
    } 
    if((((a95 == 10) && (a120.equals("f"))) && ((a30.equals("f")) && ((a41 == 2) && (((a111 == 10) && ((input.equals("D")) && cf)) && (a170 == 6)))))) {
    	cf = false;
    	a65 = "i";
    	a56 = 12;
    	a109 = 4; 
    	System.out.println("Y");
    } 
    if((((a168 == 8) && (a197.equals("f"))) && ((a120.equals("f")) && ((((cf && (input.equals("E"))) && (a196.equals("f"))) && (a92 == 5)) && (a41 == 2))))) {
    	cf = false;
    	a82 = 7;
    	a16 = "g";
    	a15 = 12;
    	a120 = "g";
    	a25 = "g";
    	a60 = "g";
    	a7 = 6;
    	a24 = "g";
    	a200 = "g";
    	a191 = "g";
    	a197 = "g";
    	a41 = 3;
    	a150 = "g";
    	a188 = 5;
    	a56 = 9;
    	a111 = 11;
    	a170 = 7;
    	a37 = 12;
    	a182 = 17; 
    	System.out.println("S");
    } 
    if(((((a16.equals("f")) && ((a79 == 9) && (a30.equals("f")))) && (a37 == 11)) && (((a164.equals("f")) && (cf && (input.equals("A")))) && (a37 == 11)))) {
    	cf = false;
    	a56 = 8;
    	a29 = "f";
    	a160 = 4; 
    	System.out.println("Y");
    } 
}
private  void calculateOutputm20(String input) {
    if((((a200.equals("f")) && ((a92 == 5) && (a7 == 5))) && ((a200.equals("f")) && ((((a116.equals("e")) && cf) && (a142 == 6)) && (a79 == 9))))) {
    	calculateOutputm99(input);
    } 
}
private  void calculateOutputm105(String input) {
    if((((a14.equals("f")) && ((((cf && (input.equals("C"))) && (a170 == 6)) && (a196.equals("f"))) && (a120.equals("f")))) && ((a164.equals("f")) && (a120.equals("f"))))) {
    	cf = false;
    	a86 = "f";
    	a39 = 3;
    	a116 = "e";
    	a160 = 3; 
    	System.out.println("Z");
    } 
    if((((a25.equals("f")) && ((a95 == 10) && (cf && (input.equals("D"))))) && ((a131.equals("f")) && (((a63.equals("f")) && (a92 == 5)) && (a30.equals("f")))))) {
    	cf = false;
    	a0 = 10;
    	a56 = 7;
    	a69 = "e";
    	a200 = "e";
    	a79 = 8;
    	a37 = 10;
    	a86 = "e";
    	a188 = 3;
    	a63 = "e";
    	a150 = "e";
    	a24 = "e";
    	a170 = 5;
    	a7 = 4;
    	a197 = "e";
    	a111 = 9;
    	a23 = 2; 
    	System.out.println("Y");
    } 
    if((((a164.equals("f")) && ((a150.equals("f")) && ((a0 == 11) && (a170 == 6)))) && ((a30.equals("f")) && ((cf && (input.equals("E"))) && (a191.equals("f")))))) {
    	cf = false;
    	a109 = 9;
    	a56 = 12;
    	a23 = 6; 
    	System.out.println("Y");
    } 
    if((((cf && (input.equals("A"))) && (a0 == 11)) && (((a120.equals("f")) && (((a82 == 6) && (a168 == 8)) && (a196.equals("f")))) && (a170 == 6)))) {
    	cf = false;
    	a56 = 8;
    	a86 = "f";
    	a160 = 7;
    	a44 = 10; 
    	System.out.println("Z");
    } 
    if(((((a92 == 5) && (a168 == 8)) && (a39 == 2)) && ((((a168 == 8) && ((input.equals("B")) && cf)) && (a37 == 11)) && (a200.equals("f"))))) {
    	cf = false;
    	a25 = "g";
    	a60 = "f";
    	a16 = "g";
    	a41 = 3;
    	a24 = "g";
    	a66 = "g";
    	a37 = 12;
    	a79 = 10;
    	a168 = 9;
    	a111 = 11;
    	a142 = 7;
    	a150 = "g";
    	a188 = 5;
    	a200 = "g";
    	a196 = "g";
    	a56 = 9; 
    	System.out.println("S");
    } 
}
private  void calculateOutputm21(String input) {
    if(((((a168 == 8) && ((a176 == 9) && cf)) && (a111 == 10)) && (((a41 == 2) && ((a191.equals("f")) && (a200.equals("f")))) && (a188 == 4)))) {
    	calculateOutputm105(input);
    } 
}
private  void calculateOutputm111(String input) {
    if(((((a25.equals("f")) && (cf && (input.equals("D")))) && (a168 == 8)) && ((a95 == 10) && (((a164.equals("f")) && (a16.equals("f"))) && (a92 == 5))))) {
    	cf = false;
    	a109 = 2;
    	a56 = 12;
    	a45 = 16; 
    	System.out.println("Y");
    } 
    if((((((a79 == 9) && (a25.equals("f"))) && (a197.equals("f"))) && (a30.equals("f"))) && (((a41 == 2) && ((input.equals("A")) && cf)) && (a200.equals("f"))))) {
    	cf = false;
    	a92 = 6;
    	a160 = 6;
    	a56 = 8;
    	a129 = 11; 
    	System.out.println("Y");
    } 
    if(((((cf && (input.equals("E"))) && (a95 == 10)) && (a0 == 11)) && (((a120.equals("f")) && ((a63.equals("f")) && (a41 == 2))) && (a15 == 11)))) {
    	cf = false;
    	a109 = 3;
    	a56 = 12;
    	a160 = 4; 
    	System.out.println("Y");
    } 
}
private  void calculateOutputm113(String input) {
    if((((a0 == 11) && (((a16.equals("f")) && ((a111 == 10) && (cf && (input.equals("B"))))) && (a24.equals("f")))) && ((a131.equals("f")) && (a63.equals("f"))))) {
    	cf = false;
    	a86 = "g";
    	a160 = 4;
    	a176 = 9; 
    	System.out.println("Y");
    } 
    if((((((input.equals("D")) && cf) && (a131.equals("f"))) && (a197.equals("f"))) && (((a39 == 2) && ((a24.equals("f")) && (a142 == 6))) && (a188 == 4)))) {
    	cf = false;
    	a23 = 1; 
    	System.out.println("S");
    } 
}
private  void calculateOutputm24(String input) {
    if((((a168 == 8) && ((a170 == 6) && ((a39 == 2) && (a16.equals("f"))))) && (((a39 == 2) && ((a23 == 1) && cf)) && (a196.equals("f"))))) {
    	calculateOutputm111(input);
    } 
    if(((((((a14.equals("f")) && (cf && (a23 == 4))) && (a30.equals("f"))) && (a150.equals("f"))) && (a41 == 2)) && ((a79 == 9) && (a25.equals("f"))))) {
    	calculateOutputm113(input);
    } 
}
private  void calculateOutputm126(String input) {
    if((((a14.equals("f")) && ((a150.equals("f")) && (a15 == 11))) && ((a15 == 11) && (((a111 == 10) && (cf && (input.equals("D")))) && (a79 == 9))))) {
    	cf = false;
    	a160 = 8;
    	a56 = 8;
    	a54 = 5; 
    	System.out.println("Y");
    } 
    if((((a131.equals("f")) && (cf && (input.equals("A")))) && ((a41 == 2) && ((a30.equals("f")) && ((a170 == 6) && ((a16.equals("f")) && (a120.equals("f")))))))) {
    	cf = false;
    	a56 = 14;
    	a38 = "g";
    	a6 = 11; 
    	System.out.println("Y");
    } 
    if(((((a92 == 5) && (a131.equals("f"))) && (a131.equals("f"))) && (((a82 == 6) && ((cf && (input.equals("C"))) && (a41 == 2))) && (a111 == 10)))) {
    	cf = false;
    	a56 = 14;
    	a38 = "g";
    	a6 = 8; 
    	System.out.println("Y");
    } 
    if(((((a95 == 10) && (a30.equals("f"))) && (a63.equals("f"))) && ((a37 == 11) && (((a150.equals("f")) && ((input.equals("B")) && cf)) && (a168 == 8))))) {
    	cf = false;
    	a84 = "g";
    	a109 = 5; 
    	System.out.println("Y");
    } 
    if((((a41 == 2) && ((a170 == 6) && ((((cf && (input.equals("E"))) && (a82 == 6)) && (a191.equals("f"))) && (a39 == 2)))) && (a95 == 10))) {
    	cf = false;
    	a56 = 10;
    	a86 = "g";
    	a160 = 4;
    	a176 = 9; 
    	System.out.println("Y");
    } 
}
private  void calculateOutputm29(String input) {
    if((((((a150.equals("f")) && ((cf && (a65.equals("g"))) && (a197.equals("f")))) && (a120.equals("f"))) && (a79 == 9)) && ((a30.equals("f")) && (a92 == 5)))) {
    	calculateOutputm126(input);
    } 
}
private  void calculateOutputm134(String input) {
    if(((((a168 == 8) && (a16.equals("f"))) && (a170 == 6)) && ((a25.equals("f")) && ((a196.equals("f")) && ((a30.equals("f")) && (cf && (input.equals("D")))))))) {
    	cf = false;
    	a131 = "f";
    	a56 = 8;
    	a197 = "f";
    	a86 = "f";
    	a188 = 4;
    	a24 = "f";
    	a15 = 11;
    	a160 = 2;
    	a36 = 10; 
    	System.out.println("W");
    } 
    if((((a191.equals("f")) && ((a39 == 2) && ((input.equals("B")) && cf))) && (((a79 == 9) && ((a63.equals("f")) && (a111 == 10))) && (a150.equals("f"))))) {
    	cf = false;
    	a56 = 8;
    	a197 = "f";
    	a188 = 4;
    	a160 = 7;
    	a86 = "f";
    	a44 = 9; 
    	System.out.println("Y");
    } 
    if((((a39 == 2) && (a0 == 11)) && ((a16.equals("f")) && ((a200.equals("f")) && (((a170 == 6) && (cf && (input.equals("A")))) && (a25.equals("f"))))))) {
    	cf = false;
    	a196 = "e";
    	a0 = 10;
    	a168 = 7;
    	a7 = 4;
    	a197 = "e";
    	a191 = "e";
    	a39 = 1;
    	a56 = 7;
    	a200 = "e";
    	a120 = "e";
    	a69 = "h";
    	a150 = "e";
    	a63 = "e";
    	a176 = 8; 
    	System.out.println("Y");
    } 
    if((((((a25.equals("f")) && (a191.equals("f"))) && (a200.equals("f"))) && (a0 == 11)) && ((a79 == 9) && ((a82 == 6) && ((input.equals("C")) && cf))))) {
    	cf = false;
    	a131 = "f";
    	a24 = "f";
    	a197 = "f";
    	a200 = "f";
    	a86 = "f";
    	a15 = 11;
    	a65 = "g";
    	a188 = 4;
    	a109 = 4; 
    	System.out.println("W");
    } 
    if(((cf && (input.equals("E"))) && ((a200.equals("f")) && ((a7 == 5) && ((a37 == 11) && ((a150.equals("f")) && ((a7 == 5) && (a191.equals("f"))))))))) {
    	cf = false;
    	a95 = 11;
    	a16 = "g";
    	a25 = "g";
    	a60 = "g";
    	a39 = 3;
    	a191 = "g";
    	a82 = 7;
    	a0 = 12;
    	a200 = "g";
    	a111 = 11;
    	a41 = 3;
    	a56 = 9;
    	a182 = 12; 
    	System.out.println("Y");
    } 
}
private  void calculateOutputm32(String input) {
    if(((((a111 == 10) && (cf && (a110 == 13))) && (a168 == 8)) && ((((a164.equals("f")) && (a41 == 2)) && (a79 == 9)) && (a142 == 6)))) {
    	calculateOutputm134(input);
    } 
}
private  void calculateOutputm137(String input) {
    if(((((a170 == 6) && (a168 == 8)) && (a25.equals("f"))) && (((a92 == 5) && ((a150.equals("f")) && ((input.equals("D")) && cf))) && (a63.equals("f"))))) {
    	cf = false;
    	a104 = "f";
    	a56 = 13;
    	a31 = 4; 
    	System.out.println("Y");
    } 
    if((((a37 == 11) && ((a95 == 10) && ((input.equals("E")) && cf))) && ((((a86.equals("f")) && (a16.equals("f"))) && (a82 == 6)) && (a168 == 8)))) {
    	cf = false;
    	a109 = 7;
    	a110 = 8; 
    	System.out.println("Y");
    } 
    if((((((cf && (input.equals("A"))) && (a95 == 10)) && (a24.equals("f"))) && (a39 == 2)) && (((a15 == 11) && (a79 == 9)) && (a111 == 10)))) {
    	cf = false;
    	a0 = 10;
    	a191 = "e";
    	a200 = "e";
    	a95 = 9;
    	a56 = 7;
    	a25 = "e";
    	a41 = 1;
    	a39 = 1;
    	a197 = "e";
    	a111 = 9;
    	a82 = 5;
    	a79 = 8;
    	a15 = 10;
    	a69 = "f";
    	a37 = 10;
    	a168 = 7;
    	a14 = "e";
    	a63 = "e";
    	a92 = 4;
    	a86 = "e";
    	a7 = 4;
    	a150 = "e";
    	a196 = "e";
    	a24 = "e";
    	a91 = 10; 
    	System.out.println("Y");
    } 
    if((((a92 == 5) && ((a24.equals("f")) && (a41 == 2))) && ((a150.equals("f")) && ((a191.equals("f")) && (((input.equals("C")) && cf) && (a15 == 11)))))) {
    	cf = false;
    	a104 = "f";
    	a197 = "f";
    	a56 = 13;
    	a31 = 8; 
    	System.out.println("Z");
    } 
    if(((cf && (input.equals("B"))) && ((a16.equals("f")) && ((((a37 == 11) && ((a150.equals("f")) && (a24.equals("f")))) && (a170 == 6)) && (a24.equals("f")))))) {
    	cf = false;
    	a16 = "g";
    	a56 = 9;
    	a191 = "g";
    	a39 = 3;
    	a79 = 10;
    	a111 = 11;
    	a60 = "g";
    	a0 = 12;
    	a200 = "g";
    	a25 = "g";
    	a24 = "g";
    	a82 = 7;
    	a188 = 5;
    	a41 = 3;
    	a182 = 10; 
    	System.out.println("Y");
    } 
}
private  void calculateOutputm33(String input) {
    if((((((a200.equals("f")) && ((a191.equals("f")) && (a86.equals("f")))) && (a79 == 9)) && (a25.equals("f"))) && ((cf && (a130 == 13)) && (a82 == 6)))) {
    	calculateOutputm137(input);
    } 
}



public  void calculateOutput(String input) {
 	cf = true;
    if((((a7 == 4) && (((a63.equals("e")) && ((a197.equals("e")) && (cf && (a56 == 7)))) && (a86.equals("e")))) && ((a0 == 10) && (a150.equals("e"))))) {
    	if((((((a200.equals("e")) && (cf && (a69.equals("f")))) && (a63.equals("e"))) && (a196.equals("e"))) && ((a95 == 9) && ((a79 == 8) && (a63.equals("e")))))) {
    		calculateOutputm2(input);
    	} 
    } 
    if((((a142 == 6) && ((a25.equals("f")) && ((a7 == 5) && (a79 == 9)))) && ((a188 == 4) && ((cf && (a56 == 8)) && (a150.equals("f")))))) {
    	if((((a164.equals("f")) && ((a86.equals("f")) && (a111 == 10))) && ((a111 == 10) && (((a95 == 10) && (cf && (a160 == 2))) && (a82 == 6))))) {
    		calculateOutputm6(input);
    	} 
    	if(((((a142 == 6) && (a142 == 6)) && (a7 == 5)) && (((a111 == 10) && ((a14.equals("f")) && ((a160 == 6) && cf))) && (a24.equals("f"))))) {
    		calculateOutputm10(input);
    	} 
    	if((((a191.equals("f")) && ((a86.equals("f")) && (((a197.equals("f")) && ((a197.equals("f")) && ((a160 == 7) && cf))) && (a37 == 11)))) && (a191.equals("f")))) {
    		calculateOutputm11(input);
    	} 
    } 
    if((((((a24.equals("g")) && ((a200.equals("g")) && ((a56 == 9) && cf))) && (a111 == 11)) && (a41 == 3)) && ((a25.equals("g")) && (a16.equals("g"))))) {
    	if((((a196.equals("g")) && (a170 == 7)) && (((a142 == 7) && (((cf && (a60.equals("h"))) && (a7 == 6)) && (a63.equals("g")))) && (a150.equals("g"))))) {
    		calculateOutputm17(input);
    	} 
    	if(((((a15 == 12) && ((a164.equals("g")) && (a0 == 12))) && (a92 == 6)) && ((a197.equals("g")) && (((a60.equals("i")) && cf) && (a63.equals("g")))))) {
    		calculateOutputm18(input);
    	} 
    } 
    if((((a168 == 8) && ((a56 == 10) && cf)) && ((a168 == 8) && ((a142 == 6) && ((a14.equals("f")) && ((a16.equals("f")) && (a170 == 6))))))) {
    	if(((((a196.equals("f")) && ((a41 == 2) && (a86.equals("f")))) && (a82 == 6)) && ((a200.equals("f")) && (((a160 == 3) && cf) && (a25.equals("f")))))) {
    		calculateOutputm20(input);
    	} 
    	if(((cf && (a160 == 4)) && ((((a79 == 9) && (((a0 == 11) && (a25.equals("f"))) && (a188 == 4))) && (a24.equals("f"))) && (a170 == 6)))) {
    		calculateOutputm21(input);
    	} 
    	if((((a196.equals("f")) && (((a30.equals("f")) && (cf && (a160 == 9))) && (a168 == 8))) && ((a39 == 2) && ((a191.equals("f")) && (a196.equals("f")))))) {
    		calculateOutputm24(input);
    	} 
    } 
    if(((((a168 == 8) && ((a14.equals("f")) && (cf && (a56 == 12)))) && (a120.equals("f"))) && (((a16.equals("f")) && (a7 == 5)) && (a170 == 6)))) {
    	if((((a131.equals("f")) && ((a41 == 2) && ((a109 == 4) && cf))) && ((a82 == 6) && (((a86.equals("f")) && (a25.equals("f"))) && (a191.equals("f")))))) {
    		calculateOutputm29(input);
    	} 
    	if((((((a191.equals("f")) && (a0 == 11)) && (a168 == 8)) && (a92 == 5)) && ((a95 == 10) && (((a109 == 7) && cf) && (a14.equals("f")))))) {
    		calculateOutputm32(input);
    	} 
    	if((((a200.equals("f")) && ((a79 == 9) && (a196.equals("f")))) && ((((a142 == 6) && ((a109 == 8) && cf)) && (a200.equals("f"))) && (a200.equals("f"))))) {
    		calculateOutputm33(input);
    	} 
    } 

    errorCheck();
    if(cf)
    	throw new IllegalArgumentException("Current state has no transition for this input!");
}


public static void main(String[] args) throws Exception {
	// init system and input reader
	Problem10 eca = new Problem10();

	// main i/o-loop
	while(true) {
		//read input
		String input = stdin.readLine();

		 if((input.equals("B")) && (input.equals("E")) && (input.equals("C")) && (input.equals("A")) && (input.equals("D")))
			throw new IllegalArgumentException("Current state has no transition for this input!");
		try {
			//operate eca engine output = 
			eca.calculateOutput(input);
		} catch(IllegalArgumentException e) {
			System.err.println("Invalid input: " + e.getMessage());
		}
	}
}
}