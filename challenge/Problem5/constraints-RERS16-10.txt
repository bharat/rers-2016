#inputs [[A, B, C, D, E, F, G, H, I, J]]
#outputs [[X, Y, Z, U, V, W, S, T, O, P, Q, R]]
#0: output P, output W responds to input I after input E until input C
(false R (! iE | ((! iI | (! iC U ((oP & ! iC) & X (! iC U oW)))) U (iC | (false R (! iI | (oP & X (true U oW))))))))

#1: input A precedes output Q before output Z
(! (true U oZ) | (! oQ U (iA | oZ)))

#2: output Q precedes output Y, output S before input A
(! (true U iA) | (! ((oY & ! iA) & X (! iA U (oS & ! iA))) U (iA | oQ)))

#3: input H precedes output V, output X after output Z
((false R ! oZ) | (! oZ U (oZ & (! (true U (oV & X (true U oX))) | (! oV U iH)))))

#4: input I precedes output Q after input E
((false R ! iE) | (true U (iE & (! oQ WU iI))))

#5: output W responds to input F before output O
(! (true U oO) | ((! iF | (! oO U (oW & ! oO))) U oO))

#6: output Y, output X without output P responds to input B after output R until output S
(false R (! oR | ((! iB | (! oS U (((oY & ! oS) & ! oP) & X ((! oS & ! oP) U oX)))) U (oS | (false R (! iB | ((oY & ! oP) & X (! oP U oX))))))))

#7: input I precedes output X between output S and output Y
(false R (! ((oS & ! oY) & (true U oY)) | (! oX U (iI | oY))))

#8: output X precedes output Y after output W until output P
(false R (oW & (! ! oP | (! oY WU (oX | oP)))))

#9: output Z responds to input C, input E between input D and output W
(false R (! (iD & (true U oW)) | ((iC & (! X (! oW U iE) | X (! oW U (iE & (true U oZ))))) U oW)))

#10: output T responds to input J after output R until output U
(false R (oR & (! ! oU | ((! iJ | (! oU U (oT & ! oU))) WU oU))))

#11: output U, output V always responds to input I
(false R (! iI | (true U (oU & X (true U oV)))))

#12: output O, output U without output X always responds to input J
(false R (! iJ | (true U ((oO & ! oX) & X (! oX U oU)))))

#13: output T, output W without output S responds to input C betwen input J and output R
(false R (! (iJ & (true U oR)) | ((! iC | (! oR U (((oT & ! oR) & ! oS) & X ((! oR & ! oS) U oW)))) U oR)))

#14: output X responds to input D, input G after output Y until output Q
(false R (! oY | ((iD & (! X (! oQ U iG) | X (! oQ U (iG & (true U oX))))) U (oQ | (false R (iD & (! X (! oQ U iG) | X (! oQ U (iG & (true U oX))))))))))

#15: output V, output P without output Q responds to input A after input B until input D
(false R (! iB | ((! iA | (! iD U (((oV & ! iD) & ! oQ) & X ((! iD & ! oQ) U oP)))) U (iD | (false R (! iA | ((oV & ! oQ) & X (! oQ U oP))))))))

#16: output Y, output Z without output W always responds to input B
(false R (! iB | (true U ((oY & ! oW) & X (! oW U oZ)))))

#17: output V always precedes output R
(! oR WU oV)

#18: output O, output W without output P responds to input I before output Z
(! (true U oZ) | ((! iI | (! oZ U (((oO & ! oZ) & ! oP) & X ((! oZ & ! oP) U oW)))) U oZ))

#19: output Y precedes output O, output V before input J
(! (true U iJ) | (! ((oO & ! iJ) & X (! iJ U (oV & ! iJ))) U (iJ | oY)))

#20: input B precedes output X, output Z before output R
(! (true U oR) | (! ((oX & ! oR) & X (! oR U (oZ & ! oR))) U (oR | iB)))

#21: output W, output Z responds to input H before output O
(! (true U oO) | ((! iH | (! oO U ((oW & ! oO) & X (! oO U oZ)))) U oO))

#22: output O precedes output U after output V
((false R ! oV) | (true U (oV & (! oU WU oO))))

#23: output X always precedes output V
(! oV WU oX)

#24: output W responds to input C before input G
(! (true U iG) | ((! iC | (! iG U (oW & ! iG))) U iG))

#25: output R, output U responds to input C before output P
(! (true U oP) | ((! iC | (! oP U ((oR & ! oP) & X (! oP U oU)))) U oP))

#26: output T, input E always precedes output S
(! (true U oS) | (! oS U ((oT & ! oS) & X (! oS U iE))))

#27: output Z responds to input B, input H after output T
(false R (! oT | (false R (iB & (! X (true U iH) | X (! iH U (iH & (true U oZ))))))))

#28: output R responds to input C, input D after output Y
(false R (! oY | (false R (iC & (! X (true U iD) | X (! iD U (iD & (true U oR))))))))

#29: output X, output Y always responds to input E
(false R (! iE | (true U (oX & X (true U oY)))))

#30: output U, output Y responds to input H between output T and output X
(false R (! (oT & (true U oX)) | ((! iH | (! oX U ((oU & ! oX) & X (! oX U oY)))) U oX)))

#31: input C precedes output R before output O
(! (true U oO) | (! oR U (iC | oO)))

#32: output W, output T responds to input C before output O
(! (true U oO) | ((! iC | (! oO U ((oW & ! oO) & X (! oO U oT)))) U oO))

#33: output Q, output T precedes output Z after input E
((false R ! iE) | (! iE U (iE & (! (true U oZ) | (! oZ U ((oQ & ! oZ) & X (! oZ U oT)))))))

#34: input F always precedes output U
(! oU WU iF)

#35: input D, input A always precedes output S
(! (true U oS) | (! oS U ((iD & ! oS) & X (! oS U iA))))

#36: output O precedes output Y between input E and output R
(false R (! ((iE & ! oR) & (true U oR)) | (! oY U (oO | oR))))

#37: input D, output X precedes output P after output Y until output T
(false R (! oY | (! (true U oP) | (! oP U (oT | ((iD & ! oP) & X (! oP U oX)))))))

#38: output T precedes output Z after input E until input D
(false R (iE & (! ! iD | (! oZ WU (oT | iD)))))

#39: output S responds to input D, input A after input J until output Q
(false R (! iJ | ((iD & (! X (! oQ U iA) | X (! oQ U (iA & (true U oS))))) U (oQ | (false R (iD & (! X (! oQ U iA) | X (! oQ U (iA & (true U oS))))))))))

#40: input C, output U precedes output P before input D
(! (true U iD) | (! oP U (iD | ((iC & ! oP) & X (! oP U oU)))))

#41: output S, input F precedes output W after output X
((false R ! oX) | (! oX U (oX & (! (true U oW) | (! oW U ((oS & ! oW) & X (! oW U iF)))))))

#42: output R, input G precedes output S after output Q
((false R ! oQ) | (! oQ U (oQ & (! (true U oS) | (! oS U ((oR & ! oS) & X (! oS U iG)))))))

#43: input H precedes output Y, output X before input G
(! (true U iG) | (! ((oY & ! iG) & X (! iG U (oX & ! iG))) U (iG | iH)))

#44: output Q responds to input A after input B until output X
(false R (iB & (! ! oX | ((! iA | (! oX U (oQ & ! oX))) WU oX))))

#45: output P responds to input G between output Q and input C
(false R (! ((oQ & ! iC) & (true U iC)) | ((! iG | (! iC U (oP & ! iC))) U iC)))

#46: output P responds to input J, input A after output Q
(false R (! oQ | (false R (iJ & (! X (true U iA) | X (! iA U (iA & (true U oP))))))))

#47: input B precedes output S, output Y between input I and output O
(false R (! (iI & (true U oO)) | (! ((oS & ! oO) & X (! oO U (oY & ! oO))) U (oO | iB))))

#48: input C precedes output Y, output V before output U
(! (true U oU) | (! ((oY & ! oU) & X (! oU U (oV & ! oU))) U (oU | iC)))

#49: input A, output Y always precedes output Z
(! (true U oZ) | (! oZ U ((iA & ! oZ) & X (! oZ U oY))))

#50: output W precedes output R, output X after output Q until input B
(false R (! oQ | ((! ((oR & ! iB) & X (! iB U (oX & ! iB))) U (iB | oW)) | (false R ! (oR & X (true U oX))))))

#51: input C precedes output T after input A
((false R ! iA) | (true U (iA & (! oT WU iC))))

#52: output W, output O precedes output S after output Y until output X
(false R (! oY | (! (true U oS) | (! oS U (oX | ((oW & ! oS) & X (! oS U oO)))))))

#53: output R, output T without output U responds to input A after input I until input E
(false R (! iI | ((! iA | (! iE U (((oR & ! iE) & ! oU) & X ((! iE & ! oU) U oT)))) U (iE | (false R (! iA | ((oR & ! oU) & X (! oU U oT))))))))

#54: output Q, output S responds to input B between output Z and input H
(false R (! (oZ & (true U iH)) | ((! iB | (! iH U ((oQ & ! iH) & X (! iH U oS)))) U iH)))

#55: output V, output X always responds to input A
(false R (! iA | (true U (oV & X (true U oX)))))

#56: input G always precedes output Z
(! oZ WU iG)

#57: output S responds to input E before input H
(! (true U iH) | ((! iE | (! iH U (oS & ! iH))) U iH))

#58: output U, output R responds to input C between output T and output X
(false R (! (oT & (true U oX)) | ((! iC | (! oX U ((oU & ! oX) & X (! oX U oR)))) U oX)))

#59: output R, output T responds to input E before output O
(! (true U oO) | ((! iE | (! oO U ((oR & ! oO) & X (! oO U oT)))) U oO))

#60: output V precedes output O, output Y before input J
(! (true U iJ) | (! ((oO & ! iJ) & X (! iJ U (oY & ! iJ))) U (iJ | oV)))

#61: output S always precedes output R, output V
(! (true U (oR & X (true U oV))) | (! oR U oS))

#62: output T responds to input D between input H and output S
(false R (! ((iH & ! oS) & (true U oS)) | ((! iD | (! oS U (oT & ! oS))) U oS)))

#63: output Z always precedes output O
(! oO WU oZ)

#64: output S responds to input I between input C and output P
(false R (! ((iC & ! oP) & (true U oP)) | ((! iI | (! oP U (oS & ! oP))) U oP)))

#65: output X precedes output W, output Q before input H
(! (true U iH) | (! ((oW & ! iH) & X (! iH U (oQ & ! iH))) U (iH | oX)))

#66: output T, output P responds to input F before output S
(! (true U oS) | ((! iF | (! oS U ((oT & ! oS) & X (! oS U oP)))) U oS))

#67: output Q responds to input H, input J before input G
(! (true U iG) | ((iH & (! X (! iG U iJ) | X (! iG U (iJ & (true U oQ))))) U iG))

#68: output S responds to input D before output Q
(! (true U oQ) | ((! iD | (! oQ U (oS & ! oQ))) U oQ))

#69: output R always responds to input G
(false R (! iG | (true U oR)))

#70: output W, input J always precedes output U
(! (true U oU) | (! oU U ((oW & ! oU) & X (! oU U iJ))))

#71: output T precedes output O, output V after output X
((false R ! oX) | (! oX U (oX & (! (true U (oO & X (true U oV))) | (! oO U oT)))))

#72: output R responds to input E before input D
(! (true U iD) | ((! iE | (! iD U (oR & ! iD))) U iD))

#73: output T, output W always precedes output O
(! (true U oO) | (! oO U ((oT & ! oO) & X (! oO U oW))))

#74: output R, output Z responds to input J before input A
(! (true U iA) | ((! iJ | (! iA U ((oR & ! iA) & X (! iA U oZ)))) U iA))

#75: output Q, output S responds to input B after input H
(false R (! iH | (false R (! iB | (oQ & X (true U oS))))))

#76: output S, output R precedes output Y after input H until output V
(false R (! iH | (! (true U oY) | (! oY U (oV | ((oS & ! oY) & X (! oY U oR)))))))

#77: output T, input F precedes output Y before output Z
(! (true U oZ) | (! oY U (oZ | ((oT & ! oY) & X (! oY U iF)))))

#78: output Z, output X without output V responds to input J after output R
(false R (! oR | (false R (! iJ | ((oZ & ! oV) & X (! oV U oX))))))

#79: output V responds to input F, input G after input E
(false R (! iE | (false R (iF & (! X (true U iG) | X (! iG U (iG & (true U oV))))))))

#80: output Q responds to input D before input I
(! (true U iI) | ((! iD | (! iI U (oQ & ! iI))) U iI))

#81: output T always responds to input C
(false R (! iC | (true U oT)))

#82: output X, output T responds to input G before output V
(! (true U oV) | ((! iG | (! oV U ((oX & ! oV) & X (! oV U oT)))) U oV))

#83: output O, output R without output W responds to input D after output U until input I
(false R (! oU | ((! iD | (! iI U (((oO & ! iI) & ! oW) & X ((! iI & ! oW) U oR)))) U (iI | (false R (! iD | ((oO & ! oW) & X (! oW U oR))))))))

#84: input D, output O precedes output V before output Z
(! (true U oZ) | (! oV U (oZ | ((iD & ! oV) & X (! oV U oO)))))

#85: output S responds to input J, input E between output O and output Y
(false R (! (oO & (true U oY)) | ((iJ & (! X (! oY U iE) | X (! oY U (iE & (true U oS))))) U oY)))

#86: output T precedes output W, output Z after input F until input J
(false R (! iF | ((! ((oW & ! iJ) & X (! iJ U (oZ & ! iJ))) U (iJ | oT)) | (false R ! (oW & X (true U oZ))))))

#87: output U responds to input E before output Z
(! (true U oZ) | ((! iE | (! oZ U (oU & ! oZ))) U oZ))

#88: output P precedes output X, output R before output O
(! (true U oO) | (! ((oX & ! oO) & X (! oO U (oR & ! oO))) U (oO | oP)))

#89: output Z, output P always precedes output O
(! (true U oO) | (! oO U ((oZ & ! oO) & X (! oO U oP))))

#90: output V responds to input G, input I after output O until input J
(false R (! oO | ((iG & (! X (! iJ U iI) | X (! iJ U (iI & (true U oV))))) U (iJ | (false R (iG & (! X (! iJ U iI) | X (! iJ U (iI & (true U oV))))))))))

#91: input B precedes output R before input G
(! (true U iG) | (! oR U (iB | iG)))

#92: output X responds to input A, input G after output V
(false R (! oV | (false R (iA & (! X (true U iG) | X (! iG U (iG & (true U oX))))))))

#93: output U, input J precedes output Y before input I
(! (true U iI) | (! oY U (iI | ((oU & ! oY) & X (! oY U iJ)))))

#94: input I precedes output P after input J until input H
(false R (iJ & (! ! iH | (! oP WU (iI | iH)))))

#95: output Q precedes output Z before input I
(! (true U iI) | (! oZ U (oQ | iI)))

#96: input D precedes output U before output Z
(! (true U oZ) | (! oU U (iD | oZ)))

#97: input J, output P always precedes output Z
(! (true U oZ) | (! oZ U ((iJ & ! oZ) & X (! oZ U oP))))

#98: output T, output X responds to input J after input G
(false R (! iG | (false R (! iJ | (oT & X (true U oX))))))

#99: output S responds to input H, input B after output P
(false R (! oP | (false R (iH & (! X (true U iB) | X (! iB U (iB & (true U oS))))))))

